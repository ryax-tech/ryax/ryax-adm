#!/usr/bin/env bash

set -e
set -u
# For debug
# set -x

# Wait for k3s to write the config
sleep 5
# Be sure that the KUBECONFIG var is set and fix k3s config
cp $KUBECONFIG /tmp/kubeconfig.yaml
sed -i 's/127.0.0.1/kubernetes/g' /tmp/kubeconfig.yaml
export KUBECONFIG=/tmp/kubeconfig.yaml

SCRIPT_DIR=$( dirname $0 )
VALUES="$SCRIPT_DIR/k3s-ryax.yaml"
LOGS_DIR="$HOME/ryax-install-logs"
mkdir -p -m 777 $LOGS_DIR

HELM_LOGS="$LOGS_DIR/helm.logs"
K8S_LOGS="$LOGS_DIR/k8s.logs"
touch $HELM_LOGS
echo "" > $HELM_LOGS

clean_exit () {
  exit_code=$?
  if [ $exit_code -gt 0 ]; then
    echo "There was an error when generating the environment. Logs are available in $LOGS_DIR."
    echo ============ BEGIN LOGS ================
    cat -v $LOGS_DIR/*
    echo ============ END LOGS ================
    echo -- Installation failed, please contact the Ryax team!
  fi
  exit $exit_code
}

trap clean_exit EXIT


echo "-- ✅ Check for Kubernetes"
echo --- Waiting for the Kubernetes API...
sleep 5
until [[ $(kubectl get endpoints/kubernetes -o=jsonpath='{.subsets[*].addresses[*].ip}') ]]; do sleep 2; echo -- Not ready yet; done
sleep 5
until [[ $(kubectl get nodes | grep ' Ready') ]]; do sleep 2; echo --- Not ready yet ; done
echo --- Kubernetes is Ready!
echo --- Waiting for the network to be UP...
until [[ $(kubectl -n kube-system get svc traefik -o jsonpath='{.status.loadBalancer.ingress[].ip}') ]]; do sleep 2; echo --- Not ready yet; done
echo "--- Network is ready!"


echo "-- 🤖 Installing Ryax..."
ryax-adm apply --values $VALUES --retry 2 --suppress-diff &>> $HELM_LOGS

echo "-- 👷 We are initializing the cluster..."
helm upgrade --install ryax-init $SCRIPT_DIR/../helm-charts/ryax-init -n ryaxns &>> $HELM_LOGS

echo "-- ⏳ Waiting for the cluster to be populated with Actions... (this can take some time)"
sleep 5
kubectl wait --for=condition=complete -n ryaxns job/ryax-init --timeout=20m &>> $K8S_LOGS
sleep 60
echo "-- 🐙 Welcome to Ryax!"
echo "Be aware that the monitoring is disabled in this deployment."
echo ""
echo "Actions are probably still building in the background, wait for them and create your first workflow!"
echo ""
echo "Your credentials:"
echo "user: user1"
echo "password: pass1"
echo "url: http://localhost/app/login"

