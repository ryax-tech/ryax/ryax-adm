#!/usr/bin/env bash

set -e

SCRIPT_DIR=$( dirname $0 )
KIND_CONFIG="$SCRIPT_DIR/kind.yaml"
VALUES="$SCRIPT_DIR/kind-ryax.yaml"

LOGS_DIR="$HOME/.cache/ryax-adm"
mkdir -p $LOGS_DIR

HELM_LOGS="$LOGS_DIR/helm.logs"
touch $HELM_LOGS
echo "" > $HELM_LOGS

KIND_LOGS="$LOGS_DIR/kind.logs"
touch $KIND_LOGS
echo "" > $KIND_LOGS

clean_exit () {
  exit_code=$?
  if [ $exit_code -gt 0 ]; then
    echo "There was an error when generating the environment. Logs are available in $LOGS_DIR."
    echo "To retry, delete the cluster first with 'kind delete cluster' and run the script again or fix the cluster configuration directly."
  fi
  exit $exit_code
}
trap clean_exit EXIT

echo "Kind cluster values :"
cat $KIND_CONFIG
echo "======================"
echo "Ryax values :"
cat $VALUES
echo "======================"

command -v ryax-adm &> /dev/null || (echo "ryax-adm was no found. Did you forget to run 'poetry shell'?" && exit 1)

echo "Creating cluster..."
kind create cluster --config=$KIND_CONFIG &>> $KIND_LOGS

echo "Installing ryax cluster..."
ryax-adm apply --values $VALUES --retry 2 --suppress-diff &>> $HELM_LOGS
echo "The cluster values are available at $PWD/$VALUES"

echo ""
echo "To take the place of a service with telepresence (ex : as the runner) :"
echo "  - telepresence connect"
echo "  - telepresence intercept ryax-runner -n ryaxns --port 8080:api --workload ryax-runner --env-file runner-intercept.env"
echo "  - export $(grep -v "^#" runner-intercept.env | xargs)"
echo "  - Run the service on your local machine"
echo ""
echo "To clean the cluster, run :"
echo "  - telepresence quit"
echo "  - kind delete cluster"
