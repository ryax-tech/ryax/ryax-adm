{ nix2container
, pkgs
, system
, appDir
, depsDir
, myTool
, python
, tag ? "latest"
, user ? "root"
, group ? "root"
, uid ? 0
, gid ? 0
, userHome ? "/root"
, withCloudAPI ? true
}:
let
  uidStr = builtins.toString uid;
  gidStr = builtins.toString gid;

  inherit (pkgs) runCommand cacert coreutils python3;
  # FIXME put the cachix pass in a secret: requires an init script...
  defaultConfig = runCommand "base-config" { } ''
    mkdir -p $out/etc/ssl/certs/
    ln -s ${cacert}/etc/ssl/certs/ca-bundle.crt $out/etc/ssl/certs/ca-certificates.crt

    # Create temporary directories
    mkdir $out/tmp
    mkdir -p $out/var/tmp

    mkdir -p $out/usr/bin
    ln -s ${coreutils}/bin/env $out/usr/bin/env

    # Fix localhost DNS resolution
    cat > $out/etc/nsswitch.conf <<EOF
    hosts: files dns
    EOF

    # create the Ryax user
    mkdir -p $out/etc/pam.d
    echo "${user}:x:${uidStr}:${gidStr}:Ryax User:${userHome}:/bin/bash" > $out/etc/passwd
    echo "${user}:!x:::::::" > $out/etc/shadow
    echo "${group}:x:${gidStr}:" > $out/etc/group
    echo "${group}:x::" > $out/etc/gshadow
    cat > $out/etc/pam.d/other <<EOF
    account sufficient pam_unix.so
    auth sufficient pam_rootok.so
    password requisite pam_unix.so nullok sha512
    session required pam_unix.so
    EOF
    touch $out/etc/login.defs
    mkdir -p $out${userHome}
  '';
  base = nix2container.buildLayer {
    perms = [
      {
        path = defaultConfig;
        regex = "/tmp";
        mode = "1777";
      }
      {
        path = defaultConfig;
        regex = "/var/tmp";
        mode = "1777";
      }
    ];

    copyToRoot =
      [
        (pkgs.buildEnv {
          name = "root";
          paths = with pkgs; [ coreutils python bashInteractive findutils procps gnutar gnugrep ];
          pathsToLink = [ "/bin" ];
        })
        defaultConfig
      ];
  };
  dependencies = nix2container.buildLayer {
    copyToRoot = runCommand "stack" { } ''
      set -x
      mkdir -p $out/data
      echo Install python environment created by pip prior to this build
      if [ -d ${depsDir}/.env ]; then
        cp -vr ${depsDir}/.env $out/data/.env
      fi
    '';
    reproducible = false;
  };
  app = nix2container.buildLayer {
    copyToRoot = runCommand "app" { } ''
      echo Install the app
      mkdir -p $out/data
      cp -vr ${appDir}/ryax ${appDir}/local_ryax ${appDir}/configs ${appDir}/helm-charts $out/data

      mkdir -p $out/bin
      cp -v ${appDir}/${myTool} $out/bin/${myTool}

      if [ -f ${appDir}/${myTool}-update-db ]
      then
        echo Database migrations detected, install it
        cp -v ${appDir}/${myTool}-update-db $out/bin/${myTool}-update-db
        cp -vr ${appDir}/migrations $out/data
        cp -v ${appDir}/alembic.ini $out/data
      fi
    '';
    reproducible = false;
  };
  extraDeps = nix2container.buildLayer {
    copyToRoot = pkgs.buildEnv {
      name = "extra";
      paths = with pkgs; [
        # basic utils
        iana-etc
        stdenv.cc.cc.lib
        curl
        gnused
        gzip

        # For cleaning and backup
        jq
        rclone
        lsof
        gawk
        skopeo
        rdfind
        pigz

        # for upgrade
        findutils
        diffutils

        # kubernetes
        kubectl
        helmfile
        kubernetes-helm
      ];
      pathsToLink = [ "/bin" "/lib" ];
    };
  };
  GPCAPIDeps = nix2container.buildLayer {
    copyToRoot = pkgs.buildEnv {
      name = "gpc";
      paths = with pkgs; [
        (
          google-cloud-sdk.withExtraComponents (
            [ google-cloud-sdk.components.gke-gcloud-auth-plugin ]
          )
        )
      ];
      pathsToLink = [ "/bin" "/lib" ];
    };
  };
  AWSAPIDeps = nix2container.buildLayer {
    copyToRoot = pkgs.buildEnv {
      name = "aws";
      paths = with pkgs; [
        # For Kubernetes login using external command
        awscli2
      ];
      pathsToLink = [ "/bin" "/lib" ];
    };
  };
  AzureAPIDeps = nix2container.buildLayer {
    copyToRoot = pkgs.buildEnv {
      name = "azure";
      paths = with pkgs; [
        azure-cli
        kubelogin
      ];
      pathsToLink = [ "/bin" "/lib" ];
    };
  };
  ScalewayAPIDeps = nix2container.buildLayer {
    copyToRoot = pkgs.buildEnv {
      name = "scaleway";
      paths = with pkgs; [
        scaleway-cli
      ];
      pathsToLink = [ "/bin" "/lib" ];
    };
  };
  helmDiff =
    let
      # TODO Change this to a pattern matching when adding more architechture or use nixpkgs helm plugins
      # See for more details: https://nixos.wiki/wiki/Helm_and_Helmfile
      arch = if system == "aarch64-linux" then "arm64" else "amd64";
      shaMap = {
        amd64 = "sha256:19zbfr483y44w5sgl2b9i52j4qfjpk59pfz8pgcl7cw4sl8b5rij";
        arm64 = "sha256:0lph10ybz1mn7qz064j3ip8hr6gw3y7nf82ms29y5g73yipbcjz0";
      };
      helmDiffVersion = "3.9.9";
      helmDiffTarball = fetchTarball {
        url = "https://github.com/databus23/helm-diff/releases/download/v${helmDiffVersion}/helm-diff-linux-${arch}.tgz";
        sha256 = shaMap."${arch}";
      };
    in
    runCommand "helm-diff" { } ''
      mkdir -p $out${userHome}/.local/share/helm/plugins/helm-diff
      cp -r ${helmDiffTarball}/* $out${userHome}/.local/share/helm/plugins/helm-diff
    '';
  helmDiffLayer = nix2container.buildLayer {
    copyToRoot = [ helmDiff ];
    perms = [
      {
        path = helmDiff;
        regex = "${userHome}/.local";
        mode = "0744";
        uid = uid;
        gid = gid;
        uname = user;
        gname = group;
      }
      {
        path = helmDiff;
        regex = "${userHome}";
        mode = "0744";
        uid = uid;
        gid = gid;
        uname = user;
        gname = group;
      }

    ];
  };
in
nix2container.buildImage {
  name = myTool;
  inherit tag;

  layers = [ base dependencies extraDeps helmDiffLayer app ] ++ (
    pkgs.lib.lists.optionals withCloudAPI [ AWSAPIDeps AzureAPIDeps ScalewayAPIDeps GPCAPIDeps ]
  );

  config.EntryPoint = [ myTool ];
  config.Env = [
    "PYTHONPATH=/data/.env:/data"
    "KUBECONFIG=/data/kubeconfig.yml"
  ];
  config.WorkingDir = "/data";
  config.Labels = {
    "ryax.tech" = myTool;
  };
  config.User = user;
}
