#!/usr/bin/env bash

set -e
# For debug
# set -x

while getopts "fd:" opt; do
  case "$opt" in
    f)
      DO_CHECK_ONLY="false"
      ;;
    d)
      CHECK_DIR="$OPTARG"
      ;;
    ?)
      echo "script usage: $(basename $0) [-f] [-d directory]" >&2
      exit 1
      ;;
  esac
done

TO_CHECK_DIR=${CHECK_DIR:-"."}
CHECK_ONLY=${DO_CHECK_ONLY:-"true"}
RUFF_EXTRA_OPTS="--fix"

if [[ $CHECK_ONLY == "true" ]]
then
    BLACK_EXTRA_OPTS="--check --diff"
    RUFF_EXTRA_OPTS="--diff"
fi

echo "-- Checking python formating"
black $TO_CHECK_DIR --exclude "docs|.poetry|.env|.venv" $BLACK_EXTRA_OPTS

echo "-- Checking python static checking"
ruff $TO_CHECK_DIR $RUFF_EXTRA_OPTS

# Skip for now
#echo "-- Checking type annotations"
#mypy $TO_CHECK_DIR/ryax --exclude '(test_*)' --show-error-codes
