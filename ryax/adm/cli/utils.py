# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import os
import sys
from pathlib import Path
from typing import Callable, List, Set, Union

import typer
from packaging import version

from ryax.adm.common import get_logger

LOG = get_logger(__name__)


class UnsupportedVersionsError(Exception):
    """
    Raised when one or several resources have versions that not supported by
    ryax-adm.
    Inputs: the unsupported resource names with the versions that were
    requested and the versions that are supported, along with the "global"
    instance version that was requested
    """

    def __init__(
        self,
        resource_names: List[str],
        versions_to_check: List[version.Version],
        supported_versions: List[version.Version],
        instance_version: version.Version,
        message: str = "",
    ):
        if len(resource_names) == 0:
            raise RuntimeError(
                "UnsupportedVersionsError requires a non null resource list"
            )
        if not len(resource_names) == len(versions_to_check) == len(supported_versions):
            raise RuntimeError(
                "UnsupportedVersionsError requires all input lists to be of the same size"
            )
        self.resource_names = resource_names
        self.versions_to_check = versions_to_check
        self.supported_versions = supported_versions
        self.instance_version = instance_version
        self.message = message

        self.versions_to_check_differ_from_instance: bool = False
        for v in versions_to_check:
            if v != instance_version:
                self.versions_to_check_differ_from_instance = True
                break

    def __str__(self) -> str:
        self.message += f"\nInstalled or requested Ryax version: {min(self.versions_to_check)}. Supported versions: {min(self.supported_versions)} and above."
        # This part is printed only if the user has altered the configuration
        # file to enter custom versions.
        if self.versions_to_check_differ_from_instance:
            self.message += "\n\nThose services have versions that are not supported:"
            for i, name in enumerate(self.resource_names):
                self.message += f"\n - {name} version {self.versions_to_check[i]} (supported from {self.supported_versions[i]})"
        return self.message


def get_include_argument(get_resource_names: Callable[[], List[str]]):
    return typer.Argument(
        None,
        metavar="[INCLUDE]...",
        help="Names of the cluster resources you wish to include in the command. Leave empty to include the entire cluster.",
        autocompletion=get_resource_names,
    )


def get_exclude_argument(get_resource_names: Callable[[], List[str]]):
    return typer.Option(
        None,
        "--exclude",
        "-e",
        help="Excludes a resource from the command. This option may be used multiple times",
        show_default=False,
        autocompletion=get_resource_names,
    )


def confirm_file_override(filename: Path, force: bool = False):
    if os.path.isfile(filename):
        # This condition cannot be factorized. We take advantage of the lazy
        # "and" evaluation
        if not force and not typer.confirm(
            f"{filename} already exists and will be deleted. Continue?"
        ):
            print("Operation Aborted.")
            sys.exit()
        os.remove(filename)


def resourceNotFound(resource_name: str, exit: bool):
    """
    On a KeyError, we want to exit on an error if --export is set. Otherwise,
    we simply warn the user.
    """
    msg = f"Couldn't find resource {resource_name}"
    if exit:
        sys.exit(msg)
    else:
        typer.secho(msg, fg=typer.colors.RED)


def get_string_subset(
    input_set: Union[Set[str], List[str]],
    include: Union[Set[str], List[str]] = set(),
    exclude: Union[Set[str], List[str]] = set(),
    exit_on_error: bool = True,
) -> List[str]:
    """
    Returns a list of string from "input_set", which names are in "include" and
    not in "exclude"
    """
    # We use sets for performance and to simpify code
    input_set = set(input_set)
    include = set(include)
    exclude = set(exclude)

    if not include <= input_set or not exclude <= input_set:
        err_message = (
            "One of the given services was not recognized (or is not available here).\n"
        )
        err_message += f"Included services : {list(include)}\n"
        err_message += f"Excluded services : {list(exclude)}\n"
        err_message += f"Available services : {list(input_set)}"
        raise ValueError(err_message)

    # Determine resources we need to exclude
    if not (include is None or len(include) == 0):
        # Verification of the input resource names
        for name in include:
            if name not in input_set:
                resourceNotFound(name, exit_on_error)
        exclude = exclude.union(input_set - include)

    return list(input_set - exclude)


def verify_backup_filename(filename: str) -> str:
    if not filename.endswith(".ryaxbackup.tgz"):
        raise typer.BadParameter("You must provide a file of type ryaxbackup.tgz")
    if not os.path.isfile(filename):
        raise typer.BadParameter(f"{os.getcwd() + filename} was not found.")
    return filename
