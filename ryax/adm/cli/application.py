# u /usr/bin/env python3

"""
Ryax administration tool CLI.
"""
import asyncio
import os
from datetime import datetime, timezone
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import List, Optional

import typer
import yaml

import ryax.adm.cli.utils as utils
from ryax.adm.actions.backup import backup as do_backup
from ryax.adm.actions.clean import clean_services
from ryax.adm.actions.cluster_config_store import (
    get_values_from_cluster,
    push_values_to_cluster,
)
from ryax.adm.actions.init import init_values
from ryax.adm.actions.restore import restore as do_restore
from ryax.adm.common import get_logger
from ryax.adm.infrastructure.helmfile import (
    get_enabled_services,
    get_ryax_services_names,
    get_services_names,
    get_stateful_services_names,
    run_helmfile_command,
)

LOG = get_logger(__name__)


app = typer.Typer(
    help="""
☢️  Ryax Administration Tool ☢️

Welcome to the ryax-adm CLI 🖖! This interface allows you to generate a
configuration file, preview your cluster and install, upgrade, or uninstall a
Ryax cluster, and much more 🚀.


Get command or subcommand usage :

ryax-adm [command] --help


For more information, see the documentation:

https://ryax-tech.gitlab.io/dev/backend/adm/


Or the internal Wiki:

https://wiki.ryax.org/en/operations/

"""
)


# Typer options
force_option = typer.Option(
    False,
    help="Don't ask for confirmation.",
)

values_file_path_option = typer.Option(
    None,
    "--values",
    help="User defined values file",
)

cluster_only: bool = typer.Option(
    False,
    "--cluster-only",
    help="Only includes cluster-wide resources.",
)

instance_only: bool = typer.Option(
    False,
    "--instance-only",
    help="Only includes instance-wide resources.",
)

scope_option: str = typer.Option(
    "",
    help="Scope of the command (cluster or instance)",
    autocompletion=lambda: ["cluster", "instance"],
)

skip_deps_option: bool = typer.Option(
    False,
    help="Skips getting updates from the chart repositories.",
)

skip_needs_option: bool = typer.Option(
    False,
    help="Bypasses dependencies between services when applying configuration.",
)

debug_option: bool = typer.Option(
    False,
    help="Debug mode for helm installs",
)


@app.command()
def version():
    """Print current version and exit."""
    import ryax.adm.version

    typer.secho(f"ryax-adm: {ryax.adm.version.__version__}", bold=True)
    typer.secho(f"Python: {ryax.adm.version.python_version}")


@app.command()
def init(
    default_values: Path = typer.Option(
        Path(),
        "--from",
        help="Default values for the configuraion.",
        show_default=False,
    ),
    from_cluster: bool = typer.Option(
        False,
        "--from-cluster",
        "-r",
        help="Get the configuration from the current Kubernetes cluster (use `kubectl config current-context` to see the current context)",
    ),
    force: bool = force_option,
    exported_values_file: str = typer.Option(
        "ryax_values.yaml",
        "--values",
        help="Custom path for the resulting values file.",
    ),
    custom_options: List[str] = typer.Option(
        None,
        "--set",
        help="Sets a value for any field of the configuration. For example, --set spec.instance.spec.version=20.09.0 is equivalent to --version 20.09.0. NB: In case of conflict, set options get precedence over the shortened options. Although, it is always better to use a shortened option because --set options do not provide any validation on the inputs.",
        show_default=False,
    ),
    remove_secrets: bool = typer.Option(
        False,
        help="Removes all secrets in the resulting values (registry credentials...)",
    ),
    minimal: bool = typer.Option(
        True,
        help="Write the entirety of default values in the config. Only applied if --from-cluster and --from aren't used.",
    ),
):
    """
    Initialyze your Ryax cluster config file.
    """
    if default_values.is_file() and from_cluster:
        raise ValueError(
            "You can't select both a file and the cluster as origin for the values file."
        )

    init_values(
        default_values,
        from_cluster,
        Path(exported_values_file),
        custom_options,
        force,
        remove_secrets,
        minimal,
    )


@app.command()
def template(
    values_file_path: str = values_file_path_option,
    include: List[str] = utils.get_include_argument(get_services_names),
    exclude: List[str] = utils.get_exclude_argument(get_services_names),
    scope: str = scope_option,
    skip_deps: bool = skip_deps_option,
    skip_needs: bool = skip_needs_option,
    debug: bool = debug_option,
):
    """
    Render the configuration template and print it.
    """
    absolute_values_path: Optional[Path] = None
    if values_file_path:
        absolute_values_path = Path(values_file_path).absolute()

    asyncio.run(
        run_helmfile_command(
            "template",
            absolute_values_path,
            get_enabled_services(get_services_names(), include, exclude),
            scope=scope,
            skip_deps=skip_deps,
            skip_needs=skip_needs,
            debug=debug,
        )
    )


@app.command()
def write_values(
    values_file_path: str = values_file_path_option,
    include: List[str] = utils.get_include_argument(get_services_names),
    exclude: List[str] = utils.get_exclude_argument(get_services_names),
    scope: str = scope_option,
    skip_deps: bool = skip_deps_option,
    skip_needs: bool = skip_needs_option,
    debug: bool = debug_option,
):
    """
    Writes the values for all services in separate files, for debugging purposes
    """
    absolute_values_path: Optional[Path] = None
    if values_file_path:
        absolute_values_path = Path(values_file_path).absolute()

    asyncio.run(
        run_helmfile_command(
            "write-values",
            absolute_values_path,
            get_enabled_services(get_services_names(), include, exclude),
            scope=scope,
            skip_deps=skip_deps,
            skip_needs=skip_needs,
            debug=debug,
        )
    )


@app.command()
def destroy(
    values_file_path: str = values_file_path_option,
    include: List[str] = utils.get_include_argument(get_services_names),
    exclude: List[str] = utils.get_exclude_argument(get_services_names),
    scope: str = scope_option,
    skip_deps: bool = skip_deps_option,
    skip_needs: bool = skip_needs_option,
    debug: bool = debug_option,
):
    """
    Uninstalls a cluster - or part of it
    """
    absolute_values_path: Optional[Path] = None
    if values_file_path:
        absolute_values_path = Path(values_file_path).absolute()

    asyncio.run(
        run_helmfile_command(
            "destroy",
            absolute_values_path,
            get_enabled_services(get_services_names(), include, exclude),
            scope=scope,
            skip_deps=skip_deps,
            skip_needs=skip_needs,
            debug=debug,
        )
    )


@app.command()
def apply(
    values_file_path: Path = typer.Option(
        ...,
        "--values",
        help="User defined values file",
    ),
    include: List[str] = utils.get_include_argument(get_services_names),
    exclude: List[str] = utils.get_exclude_argument(get_services_names),
    scope: str = scope_option,
    skip_deps: bool = skip_deps_option,
    skip_needs: bool = skip_needs_option,
    retry: int = typer.Option(
        0, help="Retry the apply this number of times. Useful for first installs."
    ),
    debug: bool = debug_option,
    suppress_diff: bool = typer.Option(
        False, help="Suppresses the diff in the output. Useful for new installs."
    ),
):
    """
    Installs or upgrade a Ryax cluster.
    """
    absolute_values_path = Path(values_file_path).absolute()

    asyncio.run(
        run_helmfile_command(
            "apply",
            absolute_values_path,
            get_enabled_services(get_services_names(), include, exclude),
            scope=scope,
            skip_deps=skip_deps,
            skip_needs=skip_needs,
            retry=retry,
            debug=debug,
            suppress_diff=suppress_diff,
        )
    )

    with open(absolute_values_path) as values_f:
        config_str = yaml.load(values_f, Loader=yaml.FullLoader)
        asyncio.run(push_values_to_cluster(config_str))
    LOG.info("🐙 Ryax configuration applied successfully!")


@app.command()
def clean(
    include: List[str] = utils.get_include_argument(get_stateful_services_names),
    exclude: List[str] = utils.get_exclude_argument(get_stateful_services_names),
    restart_services: bool = typer.Option(
        True,
        help="Whether or not we should restart services after a clean. Useful for debugging or the restore operation.",
    ),
    update_to_master: bool = typer.Option(
        False,
        help="Update all the services to master after the clean",
    ),
):
    """
    Cleans the data of given services.
    """
    selected_services = utils.get_string_subset(
        get_stateful_services_names(), include, exclude
    )
    to_clean: List[str] = []
    to_update: List[str] = []
    if len(include) == 0 and len(exclude) == 0:
        # Cleaning everything only means cleaning the pvcs. Cleaning the databases is redundant.
        to_clean = ["datastore", "filestore", "registry"]
        # Update everything to master - not just stateful services
        to_update = get_ryax_services_names()
    else:
        to_clean = selected_services
        to_update = selected_services

    asyncio.run(clean_services(to_clean, restart_services=restart_services))

    if update_to_master:
        LOG.info(f"Updating selected services to master: {to_update}")
        values = asyncio.run(get_values_from_cluster())

        for service in to_update:
            if service not in values:
                values[service] = {}
            values[service]["chartVersion"] = "0.0.0-master"

        with NamedTemporaryFile() as tmp_values:
            # Can't use yaml.dump on a temp file
            tmp_values.write(yaml.dump(values).encode())
            tmp_values.seek(0)
            asyncio.run(
                run_helmfile_command(
                    "apply",
                    Path(tmp_values.name),
                    get_enabled_services(get_services_names(), include, exclude),
                    skip_deps=False,
                    skip_needs=True,
                    suppress_diff=True,
                    scope="instance",
                )
            )

            config_str = yaml.load(tmp_values, Loader=yaml.FullLoader)
            asyncio.run(push_values_to_cluster(config_str))


@app.command()
def backup(
    force: bool = force_option,
    name: str = typer.Option(
        ...,
        "--name",
        "-n",
        help="Name for the backup. The resulting file will be called <name>-<date in ISO format>.ryaxbackup.tgz",
    ),
    destination_folder: Path = typer.Option(
        None,
        help="The backup file will be saved in this destination folder. adm will create the directory if it does not exist.",
    ),
    legacy: bool = typer.Option(
        False,
        help="Enable this option if you are backing up from a legacy cluster (before the helm migration)",
    ),
    tmp_dir: Optional[Path] = typer.Option(
        None,
        help="Custom path to a temporary folder to store the data before it's compressed. Use this in case you don't have enough space in /tmp.",
    ),
):
    """
    Backup your cluster data in a compressed file.
    """
    # Backup filename
    backup_path = Path(
        name
        + "-"
        + datetime.now(timezone.utc).replace(microsecond=0).isoformat()
        + ".ryaxbackup.tgz"
    )

    if destination_folder is not None:
        try:
            os.mkdir(destination_folder)
        except FileExistsError:
            pass
        backup_path = Path(destination_folder) / backup_path

    utils.confirm_file_override(backup_path, force)
    asyncio.run(do_backup(str(backup_path), legacy=legacy, tmp_dir=tmp_dir))
    LOG.info(f"Data was backed up to {backup_path}")


@app.command()
def restore(
    yes: bool = typer.Option(
        False,
        "--yes",
        "-y",
        help="Automatically confirms the restore operation",
    ),
    backup_filename: str = typer.Argument(
        ...,
        metavar="[FILENAME]",
        help="Path to the file (in .tgz format) containing the Ryax data to restore.",
        callback=utils.verify_backup_filename,
    ),
    clean: bool = typer.Option(
        True,
        help="Clean all services before restoring data. Disable only if you are certain that your cluster is empty and you want to save time.",
    ),
):
    """
    Restore data that was backed up in a ryaxbackup.tgz file.
    """
    if not yes and not typer.confirm(
        "Are you sure you want to continue? This action will overwrite the data in the cluster."
    ):
        print("Operation Aborted.")
        return

    asyncio.run(do_restore(backup_filename, clean))


def run():
    app()
