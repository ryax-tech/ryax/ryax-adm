# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
Manage Ryax Cluster configuration storage into the clusters.

Config files are named tech.ryax.adm.config.<version>.<revision>
Where version is the secret format version and revision is the ryax cluster
config file revision starting at 1 and increasing by one at each
install, update or upgrade
"""
import base64
import json
import tempfile
from pathlib import Path
from typing import Any, Dict, List, Tuple

import yaml

from ryax.adm.common import get_logger
from ryax.adm.infrastructure.external_command import (
    CommandFailed,
    V1ExternalCommand,
    run_external_command,
)

LOG = get_logger(__name__)


VERSION = "v1"
PREFIX = f"tech.ryax.config.{VERSION}"


class NoExistingConfiguration(Exception):
    pass


def _get_version(config_name: str) -> int:
    return int(config_name[len(PREFIX + ".") :])


def _get_latest_config_version(config_list: List[str]) -> str:
    """Get latest configuration version"""
    if len(config_list) == 0:
        raise NoExistingConfiguration(
            "Cluster config list is empty, please install ryax one time first"
        )

    latest = list(config_list)[0]
    latest_version = _get_version(latest)
    for config_name in config_list:
        version = _get_version(config_name)
        if version > latest_version:
            latest = config_name
            latest_version = version
    return latest


async def _get_latest_config_from_cluster() -> Tuple[str, str]:
    """Get the name of the latest Ryax cluster configuration from the current kubernetes cluster."""

    cmd = f"kubectl get secret -n kube-system -l {PREFIX} -o json"

    try:
        stdout, _ = await run_external_command(
            V1ExternalCommand(
                cmd,
                {},
                ["kubectl"],
            ),
            log_stdout=False,
            log_command=False,
        )
    except CommandFailed as err:
        raise Exception(
            "Failed to retreive the configuration file from the cluster. Follow the official documentation to install Ryax."
        ) from err
    secret_configs_raw = json.loads(stdout)["items"]

    secret_configs = {}
    for config_elem in secret_configs_raw:
        secret_configs[config_elem["metadata"]["name"]] = config_elem["data"]

    latest_config_name = _get_latest_config_version(secret_configs.keys())
    config_str = list(secret_configs[latest_config_name].values())[0]

    return latest_config_name, base64.b64decode(config_str).decode()


async def get_values_from_cluster() -> Dict[str, Any]:
    """
    Retrieve the latest config in the cluster. Returns a dictionnary.
    """
    _, config = await _get_latest_config_from_cluster()

    return yaml.load(config, Loader=yaml.FullLoader)


async def _push_config(config: str) -> None:
    """
    Push a ryax configuration (in the form of a string) to the cluster.
    """
    version: int = 0
    try:
        latest_config_name, latest_config = await _get_latest_config_from_cluster()
    except NoExistingConfiguration:
        version = 1
    else:
        if latest_config == config:
            LOG.info(
                "The configuration didn't change, keeping the current revision unchanged."
            )
            return
        version = _get_version(latest_config_name) + 1

    with tempfile.NamedTemporaryFile() as tmp_file:
        config_file_path: Path = Path(tmp_file.name)
        tmp_file.write(config.encode())
        tmp_file.seek(0)
        try:
            cmd = f"kubectl create secret -n kube-system generic {PREFIX}.{version} --from-file {config_file_path} && kubectl label -n kube-system secret {PREFIX}.{version} {PREFIX}={version}"
            await run_external_command(
                V1ExternalCommand(
                    cmd,
                    {},
                    ["kubectl"],
                ),
            )
        except CommandFailed as err:
            raise Exception(
                "Failed to push the configuration file to the cluster."
            ) from err
    LOG.info("Ryax configuration is now saved in the cluster")


async def push_values_to_cluster(config: Dict[str, Any]) -> None:
    """
    Pushes a config / values file in the form of a dictionnary to the cluster
    """
    config_str = yaml.dump(config)
    await _push_config(config_str)
