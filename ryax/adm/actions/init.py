# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
import os
from pathlib import Path
from typing import Any, Dict, List

import typer
import yaml

import ryax.adm.cli.utils as utils
from ryax.adm.actions.cluster_config_store import get_values_from_cluster
from ryax.adm.infrastructure.helmfile import HELMFILE_VALUES
from ryax.adm.version import RYAX_VERSION

# List of keys we should keep as minimal values
MINIMAL_VALUES = [
    "clusterName",
    "domainName",
    "storageClass",
    "tls",
    "monitoring",
    "logLevel",
    "minio.pvcSize",
    "datastore.pvcSize",
    "registry.pvcSize",
    "version",
]


def dict_from_custom_options(custom_options: List[str]) -> Dict[str, Any]:
    """
    custom_options : string list in the form ["registry.version=main",
            "clusterName=testing", ...]

    returns a dictionnary equivalent to this values list
    """
    custom_options_dict = {}

    for custom_option in custom_options:
        (raw_field, value) = custom_option.split("=")
        fields = raw_field.split(".")
        curr_element = custom_options_dict

        for i in range(len(fields) - 1):
            if fields[i] not in curr_element:
                curr_element[fields[i]] = {}
            curr_element = curr_element[fields[i]]

        true_strings = ["true", "yes"]
        false_strings = ["false", "no"]
        if value.isnumeric():
            value = int(value)
        elif value.lower() in true_strings or value.lower() in false_strings:
            value = value in true_strings

        curr_element[fields[-1]] = value

    return custom_options_dict


def merge_dicts(dict1, dict2):
    """
    Recursively merges dict2 into dict1, in place. Overwrites dict1 values if
    they are in conflict with dict2 values.

    Ex:

    dict1 = {
        "env": "dev",
        "registry": {
            "version": "master",
            "pvcSize": "20Gi"
        }
    }

    dict2 = {
        "registry": {
            "version": "staging"
        }
        "hostname": "dev-1"
    }

    merge_dicts(dict1, dict2)

    dict1
    {
        "env": "dev",
        "hostname": "dev-1"
        "registry": {
            "version": "staging",
            "pvcSize": "20Gi"
        }
    }

    """
    for key, val in dict2.items():
        if isinstance(val, dict):
            d1_val = dict1.setdefault(key, {})
            merge_dicts(d1_val, val)
        else:
            dict1[key] = val


def set_command_line_values(values: Dict[str, Any], custom_options: List[str]):
    """
    values: Dictionnary representing a ryax_config or a helmfile values file
    custom_options: List of values to set

    Side effects: modifies the input config with the given set of custom inline
    configuration.

    Ex : --set registry=release will set the
    config["defaults"]["registry"] field to "release".
    """
    merge_dicts(values, dict_from_custom_options(custom_options))


def remove_secrets_in_values(values: Dict[str, Any]) -> None:
    """
    Edits the values in place to remove the credentials
    """
    values.get("chartRegistry", {}).pop("user", None)
    values.get("chartRegistry", {}).pop("pass", None)
    values.get("appRegistry", {}).pop("user", None)
    values.get("appRegistry", {}).pop("pass", None)


def get_minimal_values(values: dict, keep: list[str]):
    """
    values : dictionary representing a yaml file
    keep : list of keys to keep in the dict. Other keys will be deleted. Keys
    can be nested.

    Example:
        Entry yaml:
            clusterName: backend
            defaults:
                pvcSize: 10Gi
                tls: true

        keep: ["defaults.tls"]

        Result:
            defaults:
                tls: true
    """
    minimal_values = {}
    for key in keep:
        split_key = key.split(".")
        if len(split_key) == 1:
            minimal_values[key] = values[key]
        else:
            minimal_nested = get_minimal_values(
                values[split_key[0]], [".".join(split_key[1:])]
            )
            if split_key[0] not in minimal_values:
                minimal_values[split_key[0]] = minimal_nested
            else:
                minimal_values[split_key[0]] |= minimal_nested
    return minimal_values


def init_values(
    default_values: Path,
    from_cluster: bool,
    destination_path: Path,
    custom_options: List[str] = [],
    force: bool = False,
    remove_secrets: bool = False,
    minimal: bool = True,
):
    # Load values either from cluster or defaults
    values: Dict[str, Any] = {}
    if from_cluster is True:
        values = asyncio.run(get_values_from_cluster())
    elif default_values.is_file():
        with open(default_values, "r") as default_f:
            values = yaml.load(default_f, Loader=yaml.FullLoader)
    else:
        with open(HELMFILE_VALUES, "r") as values_f:
            values = yaml.load(values_f, Loader=yaml.FullLoader)
            if minimal:
                values = get_minimal_values(values, MINIMAL_VALUES)
                values["version"] = RYAX_VERSION

    if remove_secrets:
        remove_secrets_in_values(values)

    # Override with inline values
    set_command_line_values(values, custom_options)

    # Dump to the destination file
    utils.confirm_file_override(destination_path, force)
    with open(destination_path, "w") as destination_f:
        yaml.dump(values, destination_f)
    # Make the file open to the world so it can be use even with when out of a
    # container running as root
    os.chmod(destination_path, 0o777)

    relative_conf_path: Path = Path()
    try:
        relative_conf_path = destination_path.relative_to(Path.cwd())
    except ValueError:
        # File path is not relative to the current directory
        relative_conf_path = destination_path
    typer.echo(f"Configuration initialized and written here: {relative_conf_path}")
