# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger
from time import sleep
from typing import List

from ryax.adm.infrastructure.helmfile import get_internal_namespace, get_user_namespace
from ryax.adm.infrastructure.kubernetes import K8sClient

logger = getLogger(__name__)


async def clean_services(services: List[str], restart_services: bool = True) -> None:
    """
    services : List of services to clean.

    restart_services : Whether or not we should restart services after a clean.
    Useful for restoring backups or debug.
    """
    k8s_client = K8sClient()
    await k8s_client.init()

    for service in services:
        logger.info(f"Cleaning {service}...")
        await preparatory_cleanup(k8s_client, service)
        await do_clean_service(k8s_client, service, restart_services)
        logger.info("Done.")
        # Wait a bit for pods to stabilize
        sleep(5)


async def preparatory_cleanup(k8s_client: K8sClient, service):
    """
    Some services require prior cleanup before emptying their database
    """
    if service == "registry":
        internal_namespace = get_internal_namespace()
        await k8s_client.delete_jobs_for_resource(internal_namespace, service)

    if service == "worker":
        user_namespace = get_user_namespace()

        await k8s_client.delete_deployments_in_namespace(user_namespace)
        await k8s_client.delete_configmaps_in_namespace(user_namespace)
        await k8s_client.delete_ingresses_in_namespace(user_namespace)
        await k8s_client.delete_services_in_namespace(user_namespace)


async def do_clean_service(k8s_client, service, restart_services: bool):
    pvcs = []
    internal_namespace = get_internal_namespace()
    try:
        # Scale down deployments
        await k8s_client.scale_deployments_for_resource(internal_namespace, service, 0)
        # Make sure the pods delete. Sometimes the scaling down can fail if the pod fails to initialize in the first place.
        await k8s_client.delete_pods_in_namespace_for_resource(
            internal_namespace, service
        )

        # Clean the service's PVCS
        pvcs = await k8s_client.get_pvcs_for_resource_as_dict(
            internal_namespace, service
        )
        await k8s_client.delete_pvcs_for_resource(
            internal_namespace, service, wait=True
        )

        if service == "datastore":
            # Restart all services so they can recreate their databases
            logger.debug("Restarting all pods")
            await k8s_client.delete_pods_in_namespace(internal_namespace)

        if service in ["runner", "authorization", "studio", "repository"]:
            await k8s_client.clean_database(internal_namespace, service)

        if service in ["worker"]:
            await k8s_client.clean_database(
                internal_namespace,
                service,
                selector="app.kubernetes.io/instance=ryax-worker,app.kubernetes.io/name=postgresql",
                auth_options="postgresql://worker:$POSTGRES_PASSWORD@ryax-worker-postgresql/postgres",
            )

        if service == "runner":
            await k8s_client.clean_filestore_bucket(
                internal_namespace, "executions-data"
            )
        if service == "studio":
            await k8s_client.clean_filestore_bucket(internal_namespace, "iodata")
    finally:
        # Recreate resources
        await k8s_client.create_pvcs_in_namespace_from_dicts(internal_namespace, pvcs)
        if restart_services:
            await k8s_client.scale_deployments_for_resource(
                internal_namespace, service, 1
            )
            await k8s_client.wait_for_pods_readiness(internal_namespace, service)
