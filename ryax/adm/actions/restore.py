# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from ryax.adm.actions.clean import clean_services
from ryax.adm.infrastructure.external_command import (
    V1ExternalCommand,
    run_external_command,
)
from ryax.adm.infrastructure.helmfile import (
    PKG_ROOT_PATH,
    get_internal_namespace,
    get_stateful_services_names,
)


async def restore(filename: str, clean: bool) -> None:
    # Clean everything prior to the restore
    if clean:
        await clean_services(get_stateful_services_names(), restart_services=True)
    internal_namespace = get_internal_namespace()
    await run_external_command(
        V1ExternalCommand(
            f"bash {PKG_ROOT_PATH}/configs/scripts/restore.sh",
            {"RYAX_INTERNAL_NAMESPACE": internal_namespace},
            ["kubectl", "rclone", "skopeo", "pigz"],
            extra_args=filename,
        )
    )
