# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from pathlib import Path
from typing import Optional

from ryax.adm.infrastructure.external_command import (
    V1ExternalCommand,
    run_external_command,
)
from ryax.adm.infrastructure.helmfile import PKG_ROOT_PATH, get_internal_namespace


async def backup(
    filename: str, legacy: bool = False, tmp_dir: Optional[Path] = None
) -> None:
    internal_namespace = get_internal_namespace()
    await run_external_command(
        V1ExternalCommand(
            f"bash {PKG_ROOT_PATH}/configs/scripts/backup.sh",
            {
                "RYAX_INTERNAL_NAMESPACE": internal_namespace,
                "LEGACY": str(legacy).lower(),
                "TMP_DIR": str(tmp_dir) if tmp_dir is not None else "",
            },
            ["kubectl", "rclone", "skopeo", "pigz", "rdfind"],
            extra_args=filename,
        )
    )
