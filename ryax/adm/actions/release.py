# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import sys

from ryax.adm.config import RegistrySpec
from ryax.adm.infrastructure.external_command import (
    V1ExternalCommand,
    run_external_command,
)


async def copy_one_image(
    image_name: str,
    dev_registry: RegistrySpec,
    prod_registry: RegistrySpec,
    dev_version: str,
    prod_version: str,
    dry_run: bool = False,
):
    """
    Copy one image from the Dev registry to the Prod registry and change its tag.
    """
    dev_img_path = f"{dev_registry.hostname}/{image_name}:{dev_version}"
    prod_img_path = f"{prod_registry.hostname}/{image_name}:{prod_version}"
    print(f"Copy image from {dev_img_path} to {prod_img_path}.", file=sys.stderr)

    cmd = V1ExternalCommand(
        "skopeo copy --insecure-policy "
        f"--src-creds '{dev_registry.credentials}' "
        f"--dest-creds '{prod_registry.credentials}' "
        f"docker://{dev_img_path} "
        f"docker://{prod_img_path}",
        env={},
        requirements=["skopeo"],
    )
    await run_external_command(cmd, dry_run=dry_run)
