import json
from logging import getLogger
from time import sleep
from typing import Any, Dict

import kubernetes_asyncio as k8s

from ryax.adm.infrastructure.external_command import (
    V1ExternalCommand,
    run_external_command,
)

logger = getLogger(__name__)


class K8sClient:
    async def init(self) -> None:
        """
        Run this method before doing anything else with the kube client.
        """
        try:
            await k8s.config.load_kube_config()
        except k8s.config.config_exception.ConfigException:
            k8s.config.load_incluster_config()

    async def delete_jobs_for_resource(
        self, namespace: str, resource_name: str
    ) -> None:
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.BatchV1Api(apiclient)
            logger.info(
                f"Deleting all jobs for resource {resource_name} in namespace {namespace}"
            )
            await api.delete_collection_namespaced_job(
                namespace=namespace,
                label_selector=f"ryax.tech/resource-name={resource_name}",
            )

    async def delete_deployments_in_namespace(self, namespace) -> None:
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.AppsV1Api(apiclient)
            logger.info(f"Deleting all deployments in namespace {namespace}")
            await api.delete_collection_namespaced_deployment(namespace=namespace)

    async def delete_configmaps_in_namespace(self, namespace: str) -> None:
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.CoreV1Api(apiclient)
            logger.info(f"Deleting all configmaps in namespace {namespace}")
            await api.delete_collection_namespaced_config_map(namespace=namespace)

    async def delete_ingresses_in_namespace(self, namespace: str) -> None:
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.NetworkingV1Api(apiclient)
            logger.info(f"Deleting all ingresses in namespace {namespace}")
            await api.delete_collection_namespaced_ingress(namespace=namespace)

    async def delete_services_in_namespace(self, namespace: str) -> None:
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.CoreV1Api(apiclient)
            logger.info(f"Deleting all services in namespace {namespace}")
            await api.delete_collection_namespaced_service(namespace=namespace)

    async def scale_deployments_for_resource(
        self, namespace: str, resource_name: str, replicas: int
    ) -> None:
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.AppsV1Api(apiclient)
            deployments_list = await api.list_namespaced_deployment(
                namespace=namespace,
                label_selector=f"ryax.tech/resource-name={resource_name}",
            )
            for deployment in deployments_list.items:
                logger.info(
                    f"Scaling {deployment.metadata.name} in namespace {namespace} to {replicas} replicas"
                )
                await api.patch_namespaced_deployment_scale(
                    namespace=namespace,
                    name=deployment.metadata.name,
                    body={"spec": {"replicas": replicas}},
                )

    async def get_pvcs_for_resource_as_dict(
        self, namespace: str, resource_name: str
    ) -> list[Dict[str, Any]]:
        """
        params:
            as_dict: bool = Return a list of dictionnaries (with camel case variables, not snake case) instead of kube objects
        """
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.CoreV1Api(apiclient)
            pvcs_list_raw = await api.list_namespaced_persistent_volume_claim(
                namespace=namespace,
                label_selector=f"ryax.tech/resource-name={resource_name}",
                _preload_content=False,
            )
            pvcs_list = json.loads(await pvcs_list_raw.content.read())
            return pvcs_list["items"]

    async def delete_pvcs_for_resource(
        self, namespace: str, resource_name: str, wait: bool = False
    ) -> None:
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.CoreV1Api(apiclient)
            logger.info(
                f"Deleting all pvcs for resource {resource_name} in namespace {namespace}"
            )
            await api.delete_collection_namespaced_persistent_volume_claim(
                namespace=namespace,
                label_selector=f"ryax.tech/resource-name={resource_name}",
            )

            if wait:
                logger.info("Waiting for pvcs deletion...")
                pvcs_list = await api.list_namespaced_persistent_volume_claim(
                    namespace=namespace,
                    label_selector=f"ryax.tech/resource-name={resource_name}",
                )
                while len(pvcs_list.items) > 0:
                    sleep(2)
                    pvcs_list = await api.list_namespaced_persistent_volume_claim(
                        namespace=namespace,
                        label_selector=f"ryax.tech/resource-name={resource_name}",
                    )

    async def create_pvcs_in_namespace_from_dicts(
        self, namespace: str, pvcs: list[Dict[str, Any]]
    ) -> None:
        """
        Creates pvcs from their bodies. Cleans unneeded metadata.
        """
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.CoreV1Api(apiclient)
            for pvc in pvcs:
                # Delete some metadata set by kube to apply again
                if pvc["spec"].get("volumeName") is not None:
                    del pvc["spec"]["volumeName"]
                if pvc["metadata"].get("uid") is not None:
                    del pvc["metadata"]["uid"]
                if pvc["metadata"].get("managedFields") is not None:
                    del pvc["metadata"]["managedFields"]
                if pvc["metadata"].get("creationTimestamp") is not None:
                    del pvc["metadata"]["creationTimestamp"]
                if pvc["metadata"].get("resourceVersion") is not None:
                    del pvc["metadata"]["resourceVersion"]
                if pvc["metadata"].get("annotations") is not None:
                    del pvc["metadata"]["annotations"]
                if pvc.get("status") is not None:
                    del pvc["status"]

                pvc_name = pvc["metadata"]["name"]
                logger.info(f"Creating PVC {pvc_name} in namespace {namespace}")

                await api.create_namespaced_persistent_volume_claim(
                    namespace=namespace, body=pvc
                )

    async def delete_pods_in_namespace(self, namespace: str) -> None:
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.CoreV1Api(apiclient)
            logger.info(f"Deleting all pods in namespace {namespace}")
            await api.delete_collection_namespaced_pod(namespace=namespace)

    async def wait_for_pods_readiness(self, namespace: str, resource: str) -> None:
        """
        Wait for all the pods of a service to be ready
        """
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.CoreV1Api(apiclient)
            logger.info(f"Waiting for all {resource} pods to be ready")
            ready = False
            # not using stream.watch because it's unavailable in kubernetes_asyncio
            while not ready:
                ready = True
                pods_list = await api.list_namespaced_pod(
                    namespace=namespace,
                    label_selector=f"ryax.tech/resource-name={resource}",
                )
                if len(pods_list.items) == 0:
                    ready = False
                for pod in pods_list.items:
                    if pod.status.phase != "Running":
                        ready = False
                if not ready:
                    sleep(5)

    async def delete_pods_in_namespace_for_resource(
        self, namespace: str, resource_name: str
    ) -> None:
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.CoreV1Api(apiclient)
            logger.info(
                f"Deleting all pods for resource {resource_name} in namespace {namespace}"
            )
            await api.delete_collection_namespaced_pod(
                namespace=namespace,
                label_selector=f"ryax.tech/resource-name={resource_name}",
            )

    async def clean_database(
        self,
        namespace: str,
        database: str,
        selector: str = "ryax.tech/resource-name=datastore,exporter=0",
        auth_options="-U ryax postgres",
    ) -> None:
        datastore_pod_name = ""
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.CoreV1Api(apiclient)
            datastore_pods_list = await api.list_namespaced_pod(
                namespace=namespace,
                label_selector=selector,
            )
            if len(datastore_pods_list.items) == 0:
                logger.warning(
                    "Unable to find the database pod, maybe it was already cleaned up."
                )
            else:
                datastore_pod_name = datastore_pods_list.items[0].metadata.name
                logger.info(f"Cleaning database {database}")

                # The following solution is what we ended up using after some trial and error with python libs. What was tried is :
                #
                # - Using the remote code execution from kubernetes_asyncio.
                # ex: https://github.com/tomplus/kubernetes_asyncio/blob/master/examples/example3.py
                # Unfortunately this method does not seem to be capable of returning the return code of the executed command. Tried with async_req=True as well.
                #
                # - Using the official kubernetes lib which supposedly has port-forwarding capabilities, but it's actually websockets and we don't have the knowledge or time to properly use them.
                # ex: https://github.com/kubernetes-client/python/blob/master/examples/pod_portforward.py
                #
                # - One other solution could be to use this third party library which actually seems to do the job : https://pypi.org/project/portforward, however when dealing with kubernetes it is wiser to stick with the official client.

                # Separating in successive calls to run_external_command allow to get the return code of each command individually. Otherwise, the whole thing is run wrapped into "/bin/sh -c" and the return code is not forwarded to us.
                await run_external_command(
                    V1ExternalCommand(
                        f"""kubectl -n {namespace} exec -i {datastore_pod_name} -- bash -s <<'EOC'
psql {auth_options} -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname='{database}';"
EOC
""",
                        {},
                        ["kubectl"],
                    ),
                    allow_failure=True,
                )
                await run_external_command(
                    V1ExternalCommand(
                        f"""kubectl -n {namespace} exec -i {datastore_pod_name} -- bash -s <<'EOC'
psql {auth_options} -c 'DROP DATABASE "{database}";'
EOC
""",
                        {},
                        ["kubectl"],
                    ),
                    allow_failure=True,
                )
                await run_external_command(
                    V1ExternalCommand(
                        f"""kubectl -n {namespace} exec -i {datastore_pod_name} -- bash -s <<'EOC'
psql {auth_options} -c 'CREATE DATABASE "{database}";'
EOC
""",
                        {},
                        ["kubectl"],
                    ),
                )
                await run_external_command(
                    V1ExternalCommand(
                        f"""kubectl -n {namespace} exec -i {datastore_pod_name} -- bash -s <<'EOC'
psql {auth_options} -c 'GRANT ALL PRIVILEGES ON DATABASE "{database}" TO "{database}";'
EOC
""",
                        {},
                        ["kubectl"],
                    ),
                )

    async def clean_filestore_bucket(self, namespace: str, bucket: str) -> None:
        filestore_pod_name = ""
        async with k8s.client.ApiClient() as apiclient:
            api = k8s.client.CoreV1Api(apiclient)
            filestore_pods_list = await api.list_namespaced_pod(
                namespace=namespace,
                label_selector="ryax.tech/resource-name=minio",
            )
            if len(filestore_pods_list.items) == 0:
                logger.warning(
                    "Unable to find the database pod, maybe it was already cleaned up."
                )
            else:
                filestore_pod_name = filestore_pods_list.items[0].metadata.name
                logger.info(f"Cleaning bucket {bucket}")
                await run_external_command(
                    V1ExternalCommand(
                        cmd=f"kubectl exec -ti -n {namespace} {filestore_pod_name} -- mc rb --force local/ryax-filestore/{bucket}/",
                        env={},
                        requirements=["kubectl"],
                    ),
                    allow_failure=True,
                )
