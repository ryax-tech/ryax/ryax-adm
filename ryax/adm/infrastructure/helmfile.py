# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import os
import re
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Any, Dict, List, Optional

import yaml

from ryax.adm.cli.utils import get_string_subset
from ryax.adm.common import PKG_ROOT_PATH, get_logger
from ryax.adm.infrastructure.external_command import (
    V1ExternalCommand,
    run_external_command,
)

LOG = get_logger(__name__)

HELMFILE_DIR = PKG_ROOT_PATH / "helm-charts"

if not os.path.isdir(HELMFILE_DIR):
    # we are in a container
    PKG_ROOT_PATH = Path("/data")
    HELMFILE_DIR = PKG_ROOT_PATH / "helm-charts"

HELMFILE_VALUES = HELMFILE_DIR / "values.yaml"
HELMFILE_FILE = HELMFILE_DIR / "helmfile.yaml"


def get_internal_namespace() -> str:
    namespace: str = ""
    with open(HELMFILE_FILE) as helmfile_f:
        # yaml.load does not work because of unquoted curly braces.
        helmfile_s = helmfile_f.read()
        # The namespace is defined at the top of the file, future references
        # are not matched
        groups = re.search(r'internalNamespace:\s?"?([a-z-]+)"?', helmfile_s)
        if groups is None:
            raise Exception(
                "Could not find a valid internal namespace in the helmfile.yaml"
            )
        namespace = groups.group(1)
    return namespace


def get_user_namespace() -> str:
    namespace: str = ""
    with open(HELMFILE_FILE) as helmfile_f:
        # yaml.load does not work because of unquoted curly braces.
        helmfile_s = helmfile_f.read()
        # The namespace is defined at the top of the file, future references
        # are not matched
        groups = re.search(r'userNamespace:\s?"?([a-z-]+)"?', helmfile_s)
        if groups is None:
            raise Exception(
                "Could not find a valid user namespace in the helmfile.yaml"
            )
        namespace = groups.group(1)
    return namespace


def load_helmfile_file() -> Dict[str, Any]:
    """
    Loads the helmfile.yaml in a dict
    """
    helmfile_d: Dict[str, Any] = {}
    with open(HELMFILE_FILE) as helmfile_f:
        # yaml.load does not work because of unquoted curly braces.
        helmfile_s = helmfile_f.read()
        # quote all templates that are not quoted
        helmfile_s = re.sub(r"((?<=:\s)\{\{.*\}\})", r"'\1'", helmfile_s)

        # There are two parts in the helmfile. The one we want has a "releases"
        # section in it.
        files = yaml.load_all(helmfile_s, Loader=yaml.FullLoader)
        for file in files:
            if "releases" in file:
                helmfile_d = file
    return helmfile_d


def get_stateful_services_names() -> List[str]:
    helmfile = load_helmfile_file()
    return [
        service["condition"].split(".")[0]
        for service in helmfile["releases"]
        if service["labels"].get("isCleanable", False)
    ]


def get_ryax_services_names() -> List[str]:
    helmfile = load_helmfile_file()
    return [
        service["condition"].split(".")[0]
        for service in helmfile["releases"]
        if service["labels"].get("ryaxService", False)
    ]


def get_services_names() -> List[str]:
    helmfile = load_helmfile_file()
    return [service["condition"].split(".")[0] for service in helmfile["releases"]]


def get_enabled_services(all_services, include, exclude) -> Dict[str, Any]:
    """
    Returns a dict describing wich service will be enabled or not, based on
    command inputs and the given values.yaml
    """
    included_services: Dict[str, Any] = {}
    for service in all_services:
        included_services[service] = {}
    if len(include) > 0:
        # If the user explicitely includes resources, override everything
        included = get_string_subset(all_services, include, exclude)
        LOG.info(f"Included services : {included}")
        for service in all_services:
            included_services[service]["enabled"] = False
        for service in included:
            included_services[service]["enabled"] = True
    else:
        # Otherwise, just disable excluded resources
        for service in exclude:
            included_services[service]["enabled"] = False
    return included_services


async def run_helmfile_command(
    command: str,
    absolute_values_path: Optional[Path],
    included_services: Dict[str, Any],
    scope: str = "",
    skip_deps: bool = False,
    skip_needs: bool = False,
    retry: int = 0,
    debug: bool = False,
    suppress_diff: bool = False,
):
    """
    Config path has to be an absolute path, because helmfile will always treat
    file paths relative to the helmfile regardless of the calling location of
    the command, which is confusing for the user.

    Runs the given helmfile command with given options, on selected resources.
    """
    helmfile_command = f"helmfile -f {HELMFILE_FILE}"
    # helmfile options go here
    if debug:
        helmfile_command += " --debug"
    # Scope of the command (instance or cluster)
    if scope:
        helmfile_command += f' -l scope="{scope}"'

    # Values given by the user
    if absolute_values_path is not None:
        if not absolute_values_path.exists():
            raise ValueError(f"Invalid values file : {absolute_values_path}")
        print(f"Loading configuration from: {absolute_values_path}")
        helmfile_command += f" --state-values-file {absolute_values_path}"

    with NamedTemporaryFile(prefix="included-services") as tmp:
        # We can't seem to be able to dump directly in a temp file with
        # yaml.dump
        tmp.write(yaml.dump(included_services).encode())
        tmp.seek(0)

        helmfile_command += f" --state-values-file {tmp.name} {command}"
        # Command options go here
        if skip_deps:
            helmfile_command += " --skip-deps"
        if skip_needs:
            helmfile_command += " --skip-needs"
        if suppress_diff:
            helmfile_command += " --suppress-diff"
        await run_external_command(
            V1ExternalCommand(
                helmfile_command,
                {},
                ["helmfile", "helm"],
            ),
            backoff_limit=retry,
        )
