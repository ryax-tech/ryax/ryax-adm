# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
import os
from dataclasses import dataclass, field
from typing import Any, Dict, List, Tuple

from ryax.adm import get_logger

LOG = get_logger(__name__)


@dataclass
class V1ExternalCommand:
    cmd: str
    env: Dict[str, str]
    requirements: List[str]
    extra_args: str = field(default="")

    def show(self, export=False, extra_args=None):
        envs = "\n".join([f"export {name}={value}" for name, value in self.env.items()])
        requ = "\n".join([f"{name}" for name in self.requirements])
        cmd = " ".join([self.cmd, self.extra_args])
        self.extra_args = extra_args if extra_args is not None else self.extra_args

        output = ""
        if export:
            for req in self.requirements:
                output += f'command -v {req} >/dev/null 2>&1 || {{ echo >&2 "ERROR: This script requires {req} to run, but it is not installed on your system.  Aborting."; exit 1; }}\n'
            output += f"\n{cmd} {self.extra_args}"
        else:
            output = f"""
Requirements:
{requ}

Commands:
{envs}
{cmd}"""
        return output


ExternalCommands = Dict[str, V1ExternalCommand]


async def _communicate_and_log(
    stream: asyncio.StreamReader, enable_log: bool = True, pid="", prefix_logs=True
) -> bytes:
    """
    Logs the output of a StreamReader. Returns the whole output once the
    streamreader is done.
    """
    prefix: str = ""
    if pid:
        prefix = f"[{pid}] "
    bytes_out: bytes = bytes()
    while not stream.at_eof():
        out = await stream.readline()
        if out:
            if enable_log:
                log = out.decode().rstrip()
                if prefix_logs:
                    LOG.info(prefix + log)
                else:
                    print(log)
            bytes_out += out
    return bytes_out


class CommandFailed(Exception):
    pass


async def run_external_command(
    command: V1ExternalCommand,
    log_stdout=True,
    log_stderr=True,
    log_command=True,
    backoff_limit=0,
    backoff_delay=5,
    dry_run=False,
    allow_failure=False,
    prefix_logs=False,
) -> Tuple[str, str]:
    """
    Runs an external command and returns (stdout, stderr).
    Raises an exception in case of error.

    Optional args:
    - log_stdout, log_stderr: prints (or not) the stdout or stderr of the command.
    - log_command: shows the command itself in the logs.
    - backoff_limit: maximum number of retries of the command.
    - backoff_delay: delay between the retries. It is multiplied by 1.5 each time.
    - dry_run: shows what the command will be but does not run it.
    - allow_failure: will return normally even if the command failed.
    - prefix_logs: if set on False, will print normally without the logs header.
    """
    cmd = command.cmd
    extra_args = command.extra_args
    extra_env = command.env
    if dry_run:
        LOG.info(f"Command: >>> {cmd} {extra_args}")
        return "", ""

    proc: asyncio.Process = None
    command_output: Tuple[bytes, bytes] = None
    retries = 0
    while True:
        try:
            # We use command.show() in order to use the dependency checks made
            # there
            proc = await asyncio.create_subprocess_shell(
                command.show(export=True, extra_args=extra_args),
                env={**(os.environ), **extra_env},
                stdout=asyncio.subprocess.PIPE,
                stderr=asyncio.subprocess.PIPE,
                shell=True,
            )
            if log_command:
                LOG.info(f"Running command: >>> [{str(proc.pid)}] {cmd} {extra_args}")

            command_output = await asyncio.gather(
                asyncio.create_task(
                    _communicate_and_log(
                        proc.stdout, log_stdout, pid=proc.pid, prefix_logs=prefix_logs
                    )
                ),
                asyncio.create_task(
                    _communicate_and_log(
                        proc.stderr,
                        log_stderr or LOG.level == LOG.debug,
                        pid=proc.pid,
                        prefix_logs=prefix_logs,
                    )
                ),
                proc.wait(),
            )
        finally:
            try:
                if proc is None:
                    raise CommandFailed(
                        f"Command could not initialize: {cmd} {extra_args}"
                    )
                proc.kill()
            except ProcessLookupError:
                pass
            if proc.returncode != 0 and retries < backoff_limit:
                LOG.info(
                    f"Command [{proc.pid}] failed. Retrying in {backoff_delay} seconds"
                )
                await asyncio.sleep(backoff_delay)
                backoff_delay *= 1.5
                retries += 1
            else:
                break
    if not allow_failure and proc.returncode != 0:
        raise CommandFailed(
            f"Shell command has failed: [{str(proc.pid)}] {cmd} {extra_args}"
        )
    return command_output[0].decode(), command_output[1].decode()


async def run_external_commands(
    external_commands: List[V1ExternalCommand],
    return_exceptions=False,
    parallelize=False,
    **kwargs,
) -> List[Any]:
    if parallelize:
        return await asyncio.gather(
            *[
                asyncio.create_task(
                    run_external_command(
                        command,
                        **kwargs,
                    )
                )
                for command in external_commands
            ],
            return_exceptions=return_exceptions,
        )
    else:
        results = []
        for command in external_commands:
            try:
                results.append(await run_external_command(command, **kwargs))
            except CommandFailed as exc:
                if return_exceptions:
                    results.append(exc)
                else:
                    raise
        return results
