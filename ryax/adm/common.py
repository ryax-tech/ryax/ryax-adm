# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from pathlib import Path

from ryax.adm import get_logger

LOG = get_logger(__name__)


PKG_ROOT_PATH: Path = Path(__file__).parent.parent.parent


class ConfigurationError(Exception):
    pass
