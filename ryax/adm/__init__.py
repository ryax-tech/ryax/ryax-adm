# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import os


def get_logger(prefix: str) -> logging.Logger:
    str_level = os.environ.get("RYAX_DEBUG", "INFO")

    numeric_level = getattr(logging, str_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % str_level)

    logging.basicConfig(
        level=numeric_level,
        format=(prefix + ": [%(levelname)7s - %(asctime)s - %(funcName)s] %(message)s"),
    )

    # Do not pollute logs with asyncio debugs
    logging.getLogger("asyncio").setLevel(logging.WARNING)
    logging.getLogger("kubernetes_asyncio").setLevel(logging.WARNING)

    logger = logging.getLogger(prefix)
    return logger
