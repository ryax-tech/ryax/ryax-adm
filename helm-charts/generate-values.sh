#!/usr/bin/env bash

set -e

TEMPLATE="$1"
SERVICE_NAME="$2"

if [ ! -f $TEMPLATE ]; then
    echo $TEMPLATE: no such file
    exit 1
fi

# We can't directly use a temp file because :
# - The file needs a .yaml.gotmpl suffix
# - OS X's mktemp does not have a --suffix option
TMP_DIR=$(mktemp -d)
RENDERED_VALUES="$TMP_DIR/$SERVICE_NAME-values.yaml.gotmpl"

sed "s/__SERVICE__/$SERVICE_NAME/g" $TEMPLATE > $RENDERED_VALUES

echo -n $RENDERED_VALUES
