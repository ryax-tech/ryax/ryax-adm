==================== Ryax Demo =======================

To activate your demo cluster NOW run:

  kubectl -n {{ .Release.Namespace }} create job --from=cronjob/{{ include "ryax-demo.fullname" . }} {{ include "ryax-demo.fullname" . }}

Then, check for the {{ include "ryax-demo.fullname" . }} job completion with:

  kubectl -n {{ .Release.Namespace }} get job {{ include "ryax-demo.fullname" . }} --watch

Once the job is completed, your Ryax cluster is now in Demo mode!

The demo user credentials are user: "demo", password "demo"
Admin user is "ryax" and its password can be found with:

  kubectl -n {{ .Release.Namespace }} get secrets ryax-user-secret -o jsonpath='{.data.admin-user-pass}' | base64 -d

WARNINGS:
- Users and projects are read only and cannot be changed even by the admin!
- Actions are rebuild, you have to wait for the action store to be populated.
- You cluster will be reset every night a 3:00
