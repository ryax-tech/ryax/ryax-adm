#!/usr/bin/env bash
set -e
set -u
set -x

echo "Create default resources"

DEFAULT_PROJECT_ID=eeee8eb0-ef38-4bcd-8ebb-c4808751fdc2
CLUSTER_URL=${CLUSTER_URL:-"https://demo.ryax.io/api"}

# TODO wait for the cluster to be fully ready
kubectl -n ryaxns rollout status deployment ryax-authorization --timeout 200s
kubectl -n ryaxns rollout status deployment ryax-studio --timeout 200s
kubectl -n ryaxns rollout status deployment ryax-repository --timeout 200s

TOKEN=$(curl --retry 18 -f --retry-all-errors --retry-delay 10 \
    "$CLUSTER_URL/authorization/login" \
    -H "Content-Type: application/json" \
    -d '{"username": "user1", "password": "pass1"}' \
    | jq '.jwt' -r)

build_all() {
    REPOSITORY_URL=$1
    REPOSITORY_NAME=$2
    REPO_ID=$(curl \
        "$CLUSTER_URL/repository/sources" \
        -X POST \
        -H "Authorization: $TOKEN" \
        -H 'Content-Type: application/json' \
        --data-raw "{\"name\":\"$REPOSITORY_NAME\",\"url\":\"$REPOSITORY_URL\",\"username\":\"\",\"password\":\"\"}" \
        | jq .id -r)

    MODULES=$(curl \
        "$CLUSTER_URL/repository/sources/$REPO_ID/scan" \
        -X POST \
        -H "Authorization: $TOKEN" \
        -H 'Content-Type: application/json' \
        | jq '.modules | .[].id' -r)

    for module in $MODULES
    do
      echo "Building $module"
      curl "$CLUSTER_URL/repository/modules/$module/build" -X POST -H "Authorization: $TOKEN"
    done

    for module in $MODULES
    do
      echo "Waiting for the modules to be built $module..."
      curl --retry 90 -f --retry-all-errors --retry-delay 10 -s "$CLUSTER_URL/studio/modules/$module" -H "Authorization: $TOKEN"
    done

}

# build default actions
build_all https://gitlab.com/ryax-tech/workflows/default-actions.git "Default Action"
# build other repositories
# TODO put this in a config map
build_all https://gitlab.com/ryax-tech/workflows/video_detection.git "Video Detection"

# import workflows from exported ZIP files
for workflow in workflows/*
do
    curl \
        "$CLUSTER_URL/studio/workflows/import" \
        -X POST \
        -H "Authorization: $TOKEN" \
        -F import_file=@$(realpath $workflow)
done

echo "Update default user name/pass"
ADMIN_ID=$(curl "$CLUSTER_URL/authorization/users" -H "Authorization: $TOKEN" | jq '.[].id' -r)
curl \
    "$CLUSTER_URL/authorization/users/$ADMIN_ID" \
    -X PUT \
    -H "Authorization: $TOKEN" \
    -H 'Content-Type: application/json' \
    --data-raw "{\"username\":\"ryax\",\"password\":\"$RYAX_ADMIN_PASS\"}"

echo "Creating the demo user"
DEMO_USER=$(curl \
    "$CLUSTER_URL/authorization/users" \
    -X POST \
    -H "Authorization: $TOKEN" \
    -H 'Content-Type: application/json' \
    --data-raw '{"username":"demo","email":"contact@ryax.tech","password":"demo","role":"Developer","comment":"Demo user"}' \
    | jq '.user_id' -r)

curl \
    "$CLUSTER_URL/authorization/projects/$DEFAULT_PROJECT_ID/user" \
    -X POST \
    -H "Authorization: $TOKEN" \
    -H 'Content-Type: application/json' \
    --data-raw "{\"user_id\":\"$DEMO_USER\"}"

echo "Make the authorization database readonly"
DATASTORE_POD="$(kubectl -n ryaxns get pods -l ryax.tech/resource-name=datastore -o jsonpath='{.items[0].metadata.name}')"
kubectl exec -ti -n ryaxns  $DATASTORE_POD -- psql -U ryax -c \
    "revoke INSERT, UPDATE, DELETE on public.project from authorization; \
    revoke INSERT, UPDATE, DELETE ON public.user from authorization;" authorization

