#!/usr/bin/env python3
import bcrypt
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("username")
parser.add_argument("password")
args = parser.parse_args()

bcrypted = bcrypt.hashpw(
    args.password.encode("utf-8"), bcrypt.gensalt(rounds=12, prefix=b"2a")
).decode("utf-8")
print(f"{args.username}:{bcrypted}", end="")
