#!/usr/bin/env python3
import base64
import os

print("R", base64.urlsafe_b64encode(os.urandom(31)).decode(), end="")
