==================== Ryax Init =======================

Setup and configure your Ryax cluster automatically!

Check for the progress by looking at {{ include "ryax-init.fullname" . }} job completion with:

  kubectl -n {{ .Release.Namespace }} get job {{ include "ryax-init.fullname" . }} --watch

Once the job is completed, your Ryax cluster is now in initialized!

WARNING: Actions are built locally, it may take some time (around 10min).
