#!/usr/bin/env bash
set -e
set -u
# For debug
#set -x

echo "Create default resources"

DEFAULT_PROJECT_ID=eeee8eb0-ef38-4bcd-8ebb-c4808751fdc2
CLUSTER_IP=$(kubectl -n kube-system get svc traefik -o jsonpath='{.status.loadBalancer.ingress[].ip}')
# AWS give a hostname not a IP
if [ -z "$CLUSTER_IP" ]
then
    CLUSTER_IP=$(kubectl -n kube-system get svc traefik -o jsonpath='{.status.loadBalancer.ingress[].hostname}')
fi

CLUSTER_URL=${CLUSTER_URL:-"https://$CLUSTER_IP/api"}
CURL="curl -k -s"

# TODO wait for the cluster to be fully ready
kubectl -n ryaxns rollout status deployment ryax-authorization --timeout 200s
kubectl -n ryaxns rollout status deployment ryax-studio --timeout 200s
kubectl -n ryaxns rollout status deployment ryax-repository --timeout 200s

TOKEN=$($CURL --retry 18 -f --retry-all-errors --retry-delay 10 \
    "$CLUSTER_URL/authorization/login" \
    -H "Content-Type: application/json" \
    -d '{"username": "user1", "password": "pass1"}' \
    | jq '.jwt' -r)

build_all() {
    REPOSITORY_URL=$1
    REPOSITORY_NAME=$2
    BRANCH=$3
    REPO_ID=$($CURL \
        "$CLUSTER_URL/repository/sources" \
        -X POST \
        -H "Authorization: $TOKEN" \
        -H 'Content-Type: application/json' \
        --data-raw "{\"name\":\"$REPOSITORY_NAME\",\"url\":\"$REPOSITORY_URL\",\"username\":\"\",\"password\":\"\"}" \
        | jq .id -r)

    ACTIONS=$($CURL \
        "$CLUSTER_URL/repository/sources/$REPO_ID/scan" \
        -X POST \
        -H "Authorization: $TOKEN" \
        -H 'Content-Type: application/json' \
        --data-raw "{\"branch\":\"$BRANCH\"}" \
        | jq '.modules | .[].name' -r)

    echo Actions to be built: $ACTIONS

    $CURL \
        "$CLUSTER_URL/repository/v2/sources/$REPO_ID/build" \
        -X POST \
        -H "Authorization: $TOKEN" \
        -H 'Content-Type: application/json'

    echo Build of all actions of $REPOSITORY_NAME is triggered.
}

# build default actions
build_all https://gitlab.com/ryax-tech/workflows/default-actions.git "Default Action" "minimal-init"

# TODO put this in a config map
# build other repositories
build_all https://gitlab.com/ryax-tech/workflows/video_detection.git "Video Detection" "master"

echo Ryax Cluster setup finished successfully!
