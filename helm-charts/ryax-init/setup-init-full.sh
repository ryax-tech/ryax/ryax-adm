#!/usr/bin/env bash
set -e
set -u
# For debug
# set -x

echo "Create default resources"

DEFAULT_PROJECT_ID=eeee8eb0-ef38-4bcd-8ebb-c4808751fdc2
CLUSTER_URL=${CLUSTER_URL:-"https://$(kubectl -n kube-system get svc traefik -o jsonpath='{.status.loadBalancer.ingress[].ip}')/api"}
CURL="curl -k -s"

# TODO wait for the cluster to be fully ready
kubectl -n ryaxns rollout status deployment ryax-authorization --timeout 200s
kubectl -n ryaxns rollout status deployment ryax-studio --timeout 200s
kubectl -n ryaxns rollout status deployment ryax-repository --timeout 200s

TOKEN=$($CURL --retry 18 -f --retry-all-errors --retry-delay 10 \
    "$CLUSTER_URL/authorization/login" \
    -H "Content-Type: application/json" \
    -d '{"username": "user1", "password": "pass1"}' \
    | jq '.jwt' -r)

build_all() {
    REPOSITORY_URL=$1
    REPOSITORY_NAME=$2
    REPO_ID=$($CURL \
        "$CLUSTER_URL/repository/sources" \
        -X POST \
        -H "Authorization: $TOKEN" \
        -H 'Content-Type: application/json' \
        --data-raw "{\"name\":\"$REPOSITORY_NAME\",\"url\":\"$REPOSITORY_URL\",\"username\":\"\",\"password\":\"\"}" \
        | jq .id -r)

    MODULES=$($CURL \
        "$CLUSTER_URL/repository/sources/$REPO_ID/scan" \
        -X POST \
        -H "Authorization: $TOKEN" \
        -H 'Content-Type: application/json' \
        | jq '.modules | .[].id' -r)

    for module in $MODULES
    do
      echo "Building $module"
      $CURL "$CLUSTER_URL/repository/modules/$module/build" -X POST -H "Authorization: $TOKEN"
    done

    for module in $MODULES
    do
      echo "Waiting for the modules to be built $module..."
      $CURL --retry 60 -f --retry-all-errors --retry-delay 10 -s "$CLUSTER_URL/studio/modules/$module" -H "Authorization: $TOKEN"
    done

}

# build default actions
build_all https://gitlab.com/ryax-tech/workflows/default-actions.git "Default Action"
# build other repositories
# TODO put this in a config map
build_all https://gitlab.com/ryax-tech/workflows/video_detection.git "Video Detection"

# import workflows from exported ZIP files
for workflow in workflows/*
do
    $CURL \
        "$CLUSTER_URL/studio/workflows/import" \
        -X POST \
        -H "Authorization: $TOKEN" \
        -F import_file=@$(realpath $workflow)
done

#echo "Update default user name/pass"
#ADMIN_ID=$($CURL "$CLUSTER_URL/authorization/users" -H "Authorization: $TOKEN" | jq '.[].id' -r)
#$CURL \
#    "$CLUSTER_URL/authorization/users/$ADMIN_ID" \
#    -X PUT \
#    -H "Authorization: $TOKEN" \
#    -H 'Content-Type: application/json' \
#    --data-raw "{\"username\":\"ryax\",\"password\":\"$RYAX_ADMIN_PASS\"}"
