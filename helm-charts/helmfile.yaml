# These values are used to render this helmfile as well as the gotmpl files.
# They are accessible with .Values (or .StateValues, which is an alias).  These
# are not to be confused with helm values: once the values files are rendered,
# you can't access helmfile values from there.
#
# If you use templates in your values files that are not supposed to be
# rendered by helmfile, you have to escape them so that helmfile renders them
# as strings.
environments:
# We only use the default environment because we can't re-use default values
# in other environments
  default:
    values:
      # Values file to be edited by the admin
      - values.yaml
      # These values are used to render the helmfile and the different values
      # files. Moving these in a separate file leads to buggy behavior.
      - brokerNameOverride: "ryax-broker"
      - brokerCookie: "ryax-broker-cookie"
      - brokerSecret: "ryax-broker-secret"
      - filestoreSecret: "ryax-minio-secret"
      - filestoreName: "ryax-filestore" # The bucket name use to store actions IOs
      - filestoreServiceName: "minio"
      - datastoreSecret: "ryax-datastore-secret"
      - grafanaSecret: "grafana-cedentials"
      - jwtSecret: "api-jwt-secret-key"
      - internalRegistryServicePort: "5000"
      - internalRegistryNodePort: "30012"
      - ryaxUserGID: 1200
      - ryaxUserUID: 1200
      - authorizationUrl: 'ryax-authorization:8080'
      # The internal namespace and user namespace values defined here are
      # hardcoded, and re-used in adm (in ryax/adm/infrastructure/helmfile.py).
      # Change with care.
      - internalNamespace: "ryaxns"
      - userNamespace: "ryaxns-execs"
      - monitoringChannel: '{{ printf "%s-%s" .Values.clusterName .Values.domainName | replace "." "-" }}'
      - hostname: '{{ .Values.clusterName }}.{{ .Values.domainName }}'
      - dashboardUrl: 'http://{{ .Values.clusterName }}.{{.Values.domainName }}:8080/d'
      - monitoringNamespace: '{{ .Values.internalNamespace }}-monitoring'
      # -- Enable registry credentials, if set to false registry access is public
      - registryCredentialsEnabled: true
      - registryPullSecretName: ryax-registry-creds-secret
---
# Set Kube version here because helm template fake the kube version which
# make the diff fail for cert-manager.
# See https://github.com/helm/helm/issues/11751
kubeVersion: "1.24.0"

helmDefaults:
  wait: false
  waitForJobs: false
  timeout: 500

repositories:

  - name: prometheus
    url: 'https://prometheus-community.github.io/helm-charts'

  - name: bitnami
    url: 'registry-1.docker.io/bitnamicharts'
    oci: true

  - name: ryax
    url: '{{ .Values.chartRegistry.url }}'
    username: '{{ sprigGet .Values.chartRegistry "user" }}'
    password: '{{ sprigGet .Values.chartRegistry "pass" }}'
    oci: true

  - name: grafana
    url: 'https://grafana.github.io/helm-charts'

  - name: jetstack
    url: 'https://charts.jetstack.io'

  - name: traefik
    url: 'https://helm.traefik.io/traefik'

releases:

  - name: 'ryax-common'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/common-resources
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.commonResources.chartVersion }}'
    disableValidation: true
    condition: commonResources.enabled
    installed: {{ .Values.commonResources.installed }}
    labels:
      scope: 'instance'
      isCleanable: false
      ryaxService: true
    needs:
      - "cert-manager/cert-manager"
    values:
      - ./common-resources-values.yaml.gotmpl
      - '{{ list "override-values.txt" "commonResources" | exec "./generate-values.sh" }}'

  - name: 'ryax-datastore'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/datastore
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.datastore.chartVersion }}'
    disableValidation: true
    condition: datastore.enabled
    installed: {{ .Values.datastore.installed }}
    labels:
      scope: 'instance'
      isCleanable: true
      ryaxService: true
    needs:
      - "{{ .Values.monitoringNamespace }}/prometheus"
    values:
      - ./datastore-values.yaml.gotmpl
      - '{{ list "override-values.txt" "datastore" | exec "./generate-values.sh" }}'

  - name: minio
    namespace: '{{ .Values.internalNamespace }}'
    chart: bitnami/minio
    version: '~12.6.4'
    condition: minio.enabled
    installed: {{ .Values.minio.installed }}
    labels:
      scope: 'instance'
      isCleanable: true
      ryaxService: false
    needs:
      - "{{ .Values.internalNamespace }}/ryax-common"
      - "{{ .Values.monitoringNamespace }}/prometheus"
    values:
      - ./minio-values.yaml.gotmpl
      - '{{ list "override-values.txt" "minio" | exec "./generate-values.sh" }}'

  - name: rabbitmq
    namespace: '{{ .Values.internalNamespace }}'
    version: '~11.13.0'
    disableValidation: true
    chart: bitnami/rabbitmq
    condition: rabbitmq.enabled
    installed: {{ .Values.rabbitmq.installed }}
    labels:
      scope: 'instance'
      isCleanable: false
      ryaxService: false
    needs:
      - "{{ .Values.internalNamespace }}/ryax-common"
      - "{{ .Values.monitoringNamespace }}/prometheus"
    values:
      - ./rabbitmq-values.yaml.gotmpl
      - '{{ list "override-values.txt" "rabbitmq" | exec "./generate-values.sh" }}'

  - name: 'ryax-front'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/front
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.front.chartVersion }}'
    disableValidation: true
    condition: front.enabled
    installed: {{ .Values.front.installed }}
    labels:
      scope: 'instance'
      isCleanable: false
      ryaxService: true
    needs:
      - "kube-system/traefik"
    values:
      - ./front-values.yaml.gotmpl
      - '{{ list "override-values.txt" "front" | exec "./generate-values.sh" }}'

  - name: 'ryax-webui'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/webui
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.webui.chartVersion }}'
    disableValidation: true
    condition: webui.enabled
    installed: {{ .Values.webui.installed }}
    labels:
      scope: 'instance'
      isCleanable: false
      ryaxService: true
    needs:
      - "kube-system/traefik"
    values:
      - ./webui-values.yaml.gotmpl
      - '{{ list "override-values.txt" "webui" | exec "./generate-values.sh" }}'

  - name: 'ryax-repository'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/repository
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.repository.chartVersion }}'
    condition: repository.enabled
    installed: {{ .Values.repository.installed }}
    disableValidation: true
    labels:
      scope: 'instance'
      isCleanable: true
      ryaxService: true
    needs:
      - "kube-system/traefik"
      - "{{ .Values.internalNamespace }}/ryax-datastore"
      - "{{ .Values.internalNamespace }}/rabbitmq"
    values:
      - ./repository.yaml.gotmpl
      - '{{ list "override-values.txt" "repository" | exec "./generate-values.sh" }}'

  - name: 'ryax-registry'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/registry
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.registry.chartVersion }}'
    disableValidation: true
    condition: registry.enabled
    installed: {{ .Values.registry.installed }}
    labels:
      scope: 'instance'
      isCleanable: true
      ryaxService: true
    needs:
      - "{{ .Values.monitoringNamespace }}/prometheus"
    values:
      - ./registry-values.yaml.gotmpl
      - '{{ list "override-values.txt" "registry" | exec "./generate-values.sh" }}'

  - name: 'ryax-action-builder'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/action-builder
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.actionBuilder.chartVersion }}'
    disableValidation: true
    condition: actionBuilder.enabled
    installed: {{ .Values.actionBuilder.installed }}
    labels:
      scope: 'instance'
      isCleanable: false
      ryaxService: true
    needs:
      - "{{ .Values.internalNamespace }}/rabbitmq"
    values:
      - ./action-builder-values.yaml.gotmpl
      - '{{ list "override-values.txt" "actionBuilder" | exec "./generate-values.sh" }}'


  - name: 'ryax-authorization'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/authorization
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.authorization.chartVersion }}'
    disableValidation: true
    condition: authorization.enabled
    installed: {{ .Values.authorization.installed }}
    labels:
      scope: 'instance'
      isCleanable: true
      ryaxService: true
    needs:
      - "kube-system/traefik"
      - "{{ .Values.internalNamespace }}/ryax-datastore"
    values:
      - ./authorization-values.yaml.gotmpl
      - '{{ list "override-values.txt" "authorization" | exec "./generate-values.sh" }}'

  - name: 'ryax-studio'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/studio
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.studio.chartVersion }}'
    disableValidation: true
    condition: studio.enabled
    installed: {{ .Values.studio.installed }}
    labels:
      scope: 'instance'
      isCleanable: true
      ryaxService: true
    needs:
      - "kube-system/traefik"
      - "{{ .Values.internalNamespace }}/ryax-datastore"
      - "{{ .Values.internalNamespace }}/rabbitmq"
    values:
      - ./studio-values.gotmpl
      - '{{ list "override-values.txt" "studio" | exec "./generate-values.sh" }}'

  - name: 'ryax-runner'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/runner
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.runner.chartVersion }}'
    disableValidation: true
    condition: runner.enabled
    installed: {{ .Values.runner.installed }}
    labels:
      scope: 'instance'
      isCleanable: true
      ryaxService: true
    needs:
      - "kube-system/traefik"
      - "{{ .Values.internalNamespace }}/ryax-common"
      - "{{ .Values.internalNamespace }}/ryax-datastore"
      - "{{ .Values.internalNamespace }}/rabbitmq"
    values:
      - ./runner-values.yaml.gotmpl
      - '{{ list "override-values.txt" "runner" | exec "./generate-values.sh" }}'

  - name: 'ryax-worker'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/worker
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.worker.chartVersion }}'
    disableValidation: true
    condition: worker.enabled
    installed: {{ .Values.worker.installed }}
    labels:
      scope: 'instance'
      isCleanable: true
      ryaxService: true
    needs:
      - "kube-system/traefik"
      - "{{ .Values.internalNamespace }}/ryax-common"
      - "{{ .Values.internalNamespace }}/rabbitmq"
    hooks:
      # We need to restart grafana to enable loki datasource
      - events:
          - "postsync"
        showlogs: true
        command: "kubectl"
        args:
          - "-n"
          - "{{ tpl .Values.monitoringNamespace . }}"
          - "delete"
          - "pod"
          - "-l"
          - "app.kubernetes.io/name=grafana"
    values:
      - ./worker-values.yaml.gotmpl
      - ./loki-stack.yaml.gotmpl
      - '{{ list "override-values.txt" "worker" | exec "./generate-values.sh" }}'

  - name: 'ryax-vpa'
    namespace: '{{ .Values.internalNamespace }}'
    chart: ryax/vpa
    version: '{{ default (default "0.0.0-latest" .Values.version) .Values.vpa.chartVersion }}'
    disableValidation: true
    condition: vpa.enabled
    installed: {{ .Values.vpa.installed }}
    labels:
      scope: 'instance'
      isCleanable: false
      ryaxService: true
    values:
      - ./vpa-values.yaml.gotmpl
      - '{{ list "override-values.txt" "vpa" | exec "./generate-values.sh" }}'

  - name: prometheus
    namespace: '{{ tpl .Values.monitoringNamespace . }}'
    createNamespace: false
    chart: prometheus/kube-prometheus-stack
    version: 51.0.0
    disableValidation: true
    condition: prometheus.enabled
    installed: {{ .Values.prometheus.installed }}
    labels:
      scope: 'cluster'
      isCleanable: false
      ryaxService: false
    needs:
      - "{{ .Values.internalNamespace }}/ryax-common"
    values:
      # - prometheus-rules.yaml.gotmpl
      - grafana.yaml.gotmpl
      - alertmanager.yaml.gotmpl
      - prometheus-operator.yaml.gotmpl
      - prometheus.yaml.gotmpl
      - '{{ list "override-values.txt" "prometheus" | exec "./generate-values.sh" }}'

  - name: traefik
    namespace: kube-system
    chart: traefik/traefik
    version: 22.1.0
    disableValidation: true
    condition: traefik.enabled
    installed: {{ .Values.traefik.installed }}
    labels:
      scope: 'cluster'
      isCleanable: false
      ryaxService: false
    values:
      - traefik-values.yaml.gotmpl
      - '{{ list "override-values.txt" "traefik" | exec "./generate-values.sh" }}'

  - name: tempo
    namespace: '{{ tpl .Values.monitoringNamespace . }}'
    createNamespace: true
    chart: grafana/tempo
    version: 1.6.1
    condition: tempo.enabled
    installed: {{ .Values.tempo.installed }}
    labels:
      scope: 'cluster'
      isCleanable: false
      ryaxService: false
    hooks:
      # We need to restart grafana to enable loki datasource
      - events:
          - "postsync"
        showlogs: true
        command: "kubectl"
        args:
          - "-n"
          - "{{ tpl .Values.monitoringNamespace . }}"
          - "delete"
          - "pod"
          - "-l"
          - "app.kubernetes.io/name=grafana"
    needs:
      - "{{ .Values.internalNamespace }}/ryax-common"
      - "{{ .Values.monitoringNamespace }}/prometheus"
    values:
      - tempo.yaml.gotmpl
      - '{{ list "override-values.txt" "tempo" | exec "./generate-values.sh" }}'

  - name: cert-manager
    namespace: cert-manager
    chart: jetstack/cert-manager
    version: 1.14.5
    disableValidation: true
    condition: certManager.enabled
    installed: {{ .Values.certManager.installed }}
    labels:
      scope: 'cluster'
      isCleanable: false
      ryaxService: false
    values:
      - installCRDs: true
      - http_proxy: '{{ env "http_proxy" }}'
      - https_proxy: '{{ env "https_proxy" }}'
      - no_proxy: '{{ env "no_proxy" }}'
      - priorityClassName: "backbone"
      - '{{ list "override-values.txt" "certManager" | exec "./generate-values.sh" }}'
