# Direct bridge between helmfile values and each service values. You can
# override any of the service's values in their "values:" section. These take
# precendence over anything.

# This one liner generates a map and therefore can't be put directly in
# helmfile.yaml releases values

{{ .Values.__SERVICE__ | get "values" nil | toYaml }}
