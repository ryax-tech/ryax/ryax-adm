#!/usr/bin/env bash

HOSTNAME=$1

RENDERED_VALUES=$(mktemp)

sed "s/__HOSTNAME__/$HOSTNAME/" prometheus-slack-configs-content.yaml > $RENDERED_VALUES

echo -n $RENDERED_VALUES

