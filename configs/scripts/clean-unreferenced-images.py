import os
import subprocess
import json
import base64
import requests
from pathlib import Path

INSTANCE = os.getenv("INSTANCE")  # Replace with input or environment variable
if not INSTANCE:
    raise ValueError("INSTANCE must be provided.")

TMP_DIR = Path("/tmp")
NAMESPACE = "ryaxns"

# Get Datastore Pod
DATASTORE_POD = (
    subprocess.check_output(
        [
            "kubectl",
            "-n",
            NAMESPACE,
            "get",
            "pods",
            "--selector",
            "ryax.tech/resource-name=datastore",
            "-o",
            "jsonpath={.items[0].metadata.name}",
        ]
    )
    .decode()
    .strip()
)

# Get Builder Pod
BUILDER_POD = (
    subprocess.check_output(
        [
            "kubectl",
            "-n",
            NAMESPACE,
            "get",
            "pods",
            "--selector",
            "ryax.tech/resource-name=action-builder",
            "-o",
            "jsonpath={.items[0].metadata.name}",
        ]
    )
    .decode()
    .strip()
)

# Get Registry Credentials
creds_secret = subprocess.check_output(
    [
        "kubectl",
        "get",
        "-n",
        f"{NAMESPACE}-execs",
        "secrets",
        "ryax-registry-creds-secret",
        "-o",
        "json",
    ]
)
creds_data = json.loads(creds_secret)
docker_config = base64.b64decode(creds_data["data"][".dockerconfigjson"]).decode()
docker_auth = json.loads(docker_config)["auths"][f"registry.{INSTANCE}"]["auth"]
creds = base64.b64decode(docker_auth).decode()

print("-- Get registry images and tags")
all_registry_tags = {}
response = requests.get(
    f"https://registry.{INSTANCE}/v2/_catalog?n=1000", auth=tuple(creds.split(":"))
)
response.raise_for_status()
registry_images = response.json()["repositories"]
for image in registry_images:
    tags_response = requests.get(
        f"https://registry.{INSTANCE}/v2/{image}/tags/list",
        auth=tuple(creds.split(":")),
    )
    tags_response.raise_for_status()
    tags = tags_response.json().get("tags", [])
    all_registry_tags[image] = tags

print("-- Number of images found:", len(all_registry_tags))

print("-- Get Ryax images and versions")
ryax_images_versions = {}
query = "\\pset tuples_only \\\\ select id, version from action;"
result = subprocess.check_output(
    [
        "kubectl",
        "-n",
        NAMESPACE,
        "exec",
        "-i",
        DATASTORE_POD,
        "--",
        "psql",
        "-q",
        "-U",
        "ryax",
        "studio",
    ],
    input=query,
    text=True,
)
for line in filter(None, result.split("\n")):
    parts = line.split("|")
    if len(parts) == 2:
        image, version = parts
        ryax_images_versions.setdefault(image.strip(), set()).add(version.strip())

print("-- Number of images in Studio:", len(ryax_images_versions))
print(ryax_images_versions)

print("-- Determining images and tags to delete")
to_delete = []
for image, tags in all_registry_tags.items():
    if tags:
        if image not in ryax_images_versions:
            to_delete.extend((image, tag) for tag in tags)
            # Check for -python3-filev1 suffix images
            filev1_image = f"{image}-python3-filev1"
            if filev1_image in all_registry_tags:
                to_delete.extend(
                    (filev1_image, tag) for tag in all_registry_tags[filev1_image]
                )
        else:
            ryax_versions = ryax_images_versions[image]
            for tag in tags:
                if tag not in ryax_versions:
                    to_delete.append((image, tag))
                    # Check for -python3-filev1 suffix images
                    filev1_image = f"{image}-python3-filev1"
                    if (
                        filev1_image in all_registry_tags
                        and tag in all_registry_tags[filev1_image]
                    ):
                        to_delete.append((filev1_image, tag))

print("-- Number of tags to be deleted:", len(to_delete))

for image, tag in to_delete:
    print(f"-- Deleting image {image}:{tag}")
    subprocess.run(
        [
            "kubectl",
            "exec",
            "-ti",
            BUILDER_POD,
            "-n",
            NAMESPACE,
            "--",
            "skopeo",
            "delete",
            f"docker://registry.{INSTANCE}/{image}:{tag}",
        ],
        check=True,
    )
    print(f"-- Deleted {image}:{tag}")

print("-- All unreferenced tags were deleted!")
