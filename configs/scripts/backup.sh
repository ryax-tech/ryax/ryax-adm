#!/usr/bin/env bash

# Downloads all data from a ryax cluster and archives it

set -e

# Env variables
#
# BACKUP_FILENAME : destination filename with the tgz extension (preferrably)
# RYAX_INTERNAL_NAMESPACE : defaults to ryaxns

NAMESPACE=${RYAX_INTERNAL_NAMESPACE:-ryaxns}
LEGACY=${LEGACY:-false}
TMP_DIR=${TMP_DIR:-""}
echo "Using namespace $NAMESPACE"

BACKUP_FILENAME=$1
[ -z "$BACKUP_FILENAME" ] && echo "BACKUP_FILENAME is not set."

# Set the temporary directory to store the data before it's compressed
if [ $TMP_DIR ]; then
  TMP_BACKUP_DIR="$TMP_DIR/${BACKUP_FILENAME%%.*}.temp"
  mkdir -p $TMP_BACKUP_DIR
else
  TMP_BACKUP_DIR=$(mktemp -d)
fi
echo "Temporary directory: $TMP_BACKUP_DIR"

# $1: tcp port
kill_connection() {
  command_index=2
  lsof_out="$(lsof -i :$1 | sed -n "${command_index}p")"
  while [ ! -z "$lsof_out" ]; do
    if [ "$(echo $lsof_out | awk '{ print $1 }')" = "kubectl" ]; then
      kill -SIGINT $(echo $lsof_out | awk '{ print $2 }')
    else
      ((command_index++))
    fi
    lsof_out="$(lsof -i :$1 | sed -n "${command_index}p")"
  done
}

# Closes connections and cleans temporary files.
clean_exit () {
  exit_code=$?
  [ $exit_code -gt 0 ] && echo "There was an error with the backup."
  echo "Cleaning files and closing connections"
  rm -rf $RCLONE_CONF
  rm -rf $TMP_BACKUP_DIR
  kill_connection 9000
  kill_connection 5000
  exit $exit_code
}

trap clean_exit EXIT

echo "Retrieving Minio credentials"
access_key_id=$(kubectl -n $NAMESPACE get secret ryax-minio-secret -o jsonpath='{.data.filestore-access}' | base64 --decode)
secret_access_key=$(kubectl -n $NAMESPACE get secret ryax-minio-secret -o jsonpath='{.data.filestore-secret}' | base64 --decode)
RCLONE_CONF=$(mktemp)
cat > $RCLONE_CONF << EOF
[minio]
type = s3
provider = Minio
env_auth = false
access_key_id = $access_key_id
secret_access_key = $secret_access_key
endpoint = http://127.0.0.1:9000
EOF

# port forward filestore and registry
kubectl port-forward -n $NAMESPACE svc/minio 9000:9000 &
kubectl port-forward -n $NAMESPACE service/ryax-registry 5000:5000 &
sleep 5

echo "Dumping SQL"
if [ $LEGACY = "true" ]; then
  DATASTORE_POD="$(kubectl -n $NAMESPACE get pod -l ryax.tech/resource-name=ryax-postgres-datastore -o jsonpath='{.items[0].metadata.name}')"
else
  DATASTORE_POD="$(kubectl -n $NAMESPACE get pod -l ryax.tech/resource-name=datastore -l exporter=0 -o jsonpath='{.items[0].metadata.name}')"
fi
kubectl -n $NAMESPACE exec $DATASTORE_POD -- pg_dumpall -U ryax > "$TMP_BACKUP_DIR/dump-ryax-cluster.sql"

echo "Copying filestore content"
objects=$(rclone --config $RCLONE_CONF lsf minio:)
for f in $objects; do
  rclone --config $RCLONE_CONF copy minio:$f "$TMP_BACKUP_DIR/minio/$f"
done

echo "Copying registry content"
mkdir -p $TMP_BACKUP_DIR/registry
for image in $(echo "\\pset tuples_only \\\\ select id from action;" | kubectl -n $NAMESPACE exec -i $DATASTORE_POD -- psql -q -U ryax studio | tr -d "\n"); do
    version=$(echo "\\pset tuples_only \\\\ select version from action where id='$image';" | kubectl -n $NAMESPACE exec -i $DATASTORE_POD -- psql -q -U ryax studio | cut -d' ' -f2)
    echo IMAGE found: $image:$version
    skopeo --insecure-policy copy --src-tls-verify=false "docker://localhost:5000/$image:$version" dir:$TMP_BACKUP_DIR/registry/$image.$version
done

echo Deduplicate the backup
rdfind -removeidentinode true -makehardlinks true $TMP_BACKUP_DIR

echo "Creating the archive"
tar --no-wildcards --force-local --use-compress-program=pigz -cvf $BACKUP_FILENAME -C $TMP_BACKUP_DIR .
