#!usr/bin/env bash
set -u
set -e

# Variables
#
# REGISTRY_USER
# REGISTRY_PASS
# REGISTRY_PATH
# MODE (install, uninstall, upgrade, diff)
# VALUES
# RYAX_INTERNAL_NAMESPACE
# RELEASE_NAME
# CHART_NAME

REPO_NAME="ryaxregistry"
NAMESPACE=${RYAX_INTERNAL_NAMESPACE-ryaxns}

REGISTRY_BASE=${REGISTRY_PATH%/*}
REGISTRY_SUFFIX=${REGISTRY_PATH##*/}
helm repo remove $REPO_NAME
helm repo add --username "$REGISTRY_USER" --password "$REGISTRY_PASS" $REPO_NAME "https://$REGISTRY_BASE/chartrepo/$REGISTRY_SUFFIX"
helm repo update

HELM_BASE_OPTIONS="$RELEASE_NAME $REPO_NAME/$CHART_NAME --namespace=$NAMESPACE"

if [ "$MODE" = "install" ]; then
  helm install $HELM_BASE_OPTIONS $VALUES
elif [ "$MODE" = "dry-run" ]; then
  helm upgrade --dry-run --debug $HELM_BASE_OPTIONS $VALUES
elif [ "$MODE" = "uninstall" ]; then
  helm delete \
    $RELEASE_NAME \
    --namespace="$NAMESPACE"
elif [ "$MODE" = "upgrade" ]; then
  helm upgrade $HELM_BASE_OPTIONS $VALUES
elif [ "$MODE" = "diff" ]; then
  helm diff --help &> /dev/null || (echo "Helm diff plugin is not installed on this machine. To install: helm plugin install https://github.com/databus23/helm-diff" && exit 1)
  helm diff upgrade --allow-unreleased $HELM_BASE_OPTIONS $VALUES
else
  echo "Unsupported operation: $MODE"
  exit 1
fi
