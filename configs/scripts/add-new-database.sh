#!/usr/bin/env bash

# Adds a new database to a live cluster that already has some databases

DATABASE=${1-$POSTGRES_DB}
NAMESPACE=${INTERNAL_NAMESPACE-ryaxns}

set -e
set -u

DATASTORE_POD=$(kubectl -n $NAMESPACE get pod -l ryax.tech/resource-name=datastore -o json | jq ".items[0].metadata.name" -r)
DATASTORE_SECRET=$(kubectl -n $NAMESPACE get secret -l ryax.tech/resource-name=datastore -o json | jq ".items[0].metadata.name" -r)

POSTGRES_DB=$DATABASE
POSTGRES_USER=$DATABASE
POSTGRES_PASS=$(kubectl -n $NAMESPACE get secret $DATASTORE_SECRET -o json | jq .data."\"datastore-$DATABASE-pass\"" -r | base64 -d)

kubectl -n $NAMESPACE exec $DATASTORE_POD -- bash -c "
psql -v --username ryax --dbname postgres <<-EOSQL
CREATE USER \"$POSTGRES_USER\";
CREATE DATABASE \"$POSTGRES_DB\";
GRANT ALL PRIVILEGES ON DATABASE \"$POSTGRES_DB\" TO \"$POSTGRES_USER\";
ALTER USER \"$POSTGRES_USER\" PASSWORD '$POSTGRES_PASS';
EOSQL
"
