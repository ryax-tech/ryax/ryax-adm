#!/usr/bin/env bash

# Restores the state of a ryax cluster from an archive

set -e

# Env variables
#
# BACKUP_FILENAME : destination filename with the tgz extension (preferrably)
# RYAX_INTERNAL_NAMESPACE : defaults to ryaxns

NAMESPACE=${RYAX_INTERNAL_NAMESPACE:-ryaxns}
echo "Using namespace $NAMESPACE"

BACKUP_FILENAME=$1
[ -z "$BACKUP_FILENAME" ] && echo "BACKUP_FILENAME is not set."

# $1: tcp port
kill_connection() {
  command_index=2
  lsof_out="$(lsof -i :$1 | sed -n "${command_index}p")"
  while [ ! -z "$lsof_out" ]; do
    if [ "$(echo $lsof_out | awk '{ print $1 }')" = "kubectl" ]; then
      kill -SIGINT $(echo $lsof_out | awk '{ print $2 }')
    else
      ((command_index++))
    fi
    lsof_out="$(lsof -i :$1 | sed -n "${command_index}p")"
  done
}

# Closes connections and cleans temporary files.
clean_exit () {
  exit_code=$?
  [ $exit_code -gt 0 ] && echo "There was an error with the restore."
  echo "Cleaning files and closing connections"
  rm -rf $RCLONE_CONF
  rm -rf $TMP_BACKUP_DIR
  kill_connection 9000
  kill_connection 5000

  # redeploy everything
  kubectl -n $NAMESPACE scale deployments --all --replicas 1 || true
  exit $exit_code
}

trap clean_exit EXIT

# Uncompress the archive
TMP_BACKUP_DIR=$(mktemp -d)
echo $TMP_BACKUP_DIR
tar --force-local --no-wildcards --use-compress-program=pigz -xvf $BACKUP_FILENAME -C $TMP_BACKUP_DIR

# Deleting deployments so the connection to the database is closed (except the datastore, filestore, registry).
kubectl -n $NAMESPACE scale deployments --all --replicas 0 || true
for service in minio registry datastore; do
  kubectl -n $NAMESPACE scale deployments -l ryax.tech/resource-name=$service --replicas 1 || true
done
kubectl -n ryaxns get deploy --output name | xargs -n1 kubectl -n $NAMESPACE rollout status

echo "Retrieving Minio credentials"
access_key_id=$(kubectl -n $NAMESPACE get secret ryax-minio-secret -o jsonpath='{.data.filestore-access}' | base64 --decode)
secret_access_key=$(kubectl -n $NAMESPACE get secret ryax-minio-secret -o jsonpath='{.data.filestore-secret}' | base64 --decode)
RCLONE_CONF=$(mktemp)
cat > $RCLONE_CONF << EOF
[minio]
type = s3
provider = Minio
env_auth = false
access_key_id = $access_key_id
secret_access_key = $secret_access_key
endpoint = http://127.0.0.1:9000
EOF

# port forward filestore and registry
kubectl port-forward -n $NAMESPACE svc/minio 9000:9000 &
kubectl port-forward -n $NAMESPACE service/ryax-registry 5000:5000 &
sleep 5

# Restore the datastore
echo "Restoring the datastore..."
DATASTORE_POD="$(kubectl -n $NAMESPACE get pod -l ryax.tech/resource-name=datastore -l exporter=0 -o jsonpath='{.items[0].metadata.name}')"
kubectl -n $NAMESPACE cp $TMP_BACKUP_DIR/dump-ryax-cluster.sql $DATASTORE_POD:/tmp/dump-ryax-cluster.sql
kubectl -n $NAMESPACE exec $DATASTORE_POD -- bash -c "
psql -v --username ryax --dbname postgres <<-EOSQL
select 'drop table if exists "' || tablename || '" cascade;'
  from pg_tables;
EOSQL
"
kubectl -n $NAMESPACE exec $DATASTORE_POD -- psql -q -U ryax postgres -f /tmp/dump-ryax-cluster.sql
kubectl -n $NAMESPACE exec $DATASTORE_POD -- rm /tmp/dump-ryax-cluster.sql

echo "Restoring the filestore..."
for file in $(ls $TMP_BACKUP_DIR/minio); do
  rclone --config $RCLONE_CONF copy "$TMP_BACKUP_DIR/minio/$file" minio:$file
done

echo "Restoring the registry..."
for image in $(ls $TMP_BACKUP_DIR/registry); do
  name=${image%%.*}
  version=${image#*.}
  skopeo --insecure-policy copy --dest-tls-verify=false dir:$TMP_BACKUP_DIR/registry/$image "docker://localhost:5000/$name:$version"
done
