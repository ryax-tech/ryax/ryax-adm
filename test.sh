#!/usr/bin/env bash
set -e
set -u

clean_up() {
    set +e
    echo "Cleaning testing environment..."
    docker-compose down -v
}
trap clean_up EXIT

export KUBECONFIG="$PWD/kubeconfig.yaml"

docker-compose up -d

echo Wait for Kubernetes API...
until [[ $(kubectl get endpoints/kubernetes -n default --kubeconfig=$KUBECONFIG -o=jsonpath='{.subsets[*].addresses[*].ip}') ]]
do
    sleep 2
    kubectl config get-contexts --kubeconfig=$KUBECONFIG || true
    docker-compose ps -a
done
echo Kubernetes Connection is UP!

pytest --cov ./ --cov-report xml:coverage.xml --cov-report term --cov-config=.coveragerc --cov-branch -ra -v $@
