from typing import Any, Dict, List

import pytest
import yaml

from ryax.adm.actions.init import (
    dict_from_custom_options,
    get_minimal_values,
    set_command_line_values,
)


@pytest.mark.parametrize(
    "input_dict, values, result_dict",
    [
        (
            {"environment": "prod"},
            ["environment=prod"],
            {"environment": "prod"},
        ),
        (
            {"number": 1},
            ["number=2"],
            {"number": 2},
        ),
        (
            {"boolean": False},
            ["boolean=true"],
            {"boolean": True},
        ),
        (
            {"environment": "dev"},
            ["environment=prod"],
            {"environment": "prod"},
        ),
        (
            {"environment": "dev", "clusterName": "testing"},
            ["environment=prod", "clusterName=staging"],
            {"environment": "prod", "clusterName": "staging"},
        ),
        (
            {"defaults": {"registry": "dev"}},
            ["defaults.registry=prod"],
            {"defaults": {"registry": "prod"}},
        ),
        (
            {"services": {"registry": {"version": "master"}}},
            ["services.registry.version=staging"],
            {"services": {"registry": {"version": "staging"}}},
        ),
        (
            {"defaults": {"registry": "dev"}},
            ["clusterName=dev"],
            {"clusterName": "dev", "defaults": {"registry": "dev"}},
        ),
        (
            {},
            ["registry.version=master"],
            {"registry": {"version": "master"}},
        ),
        (
            {"registry": {"version": "master", "pvcSize": "20Gi"}},
            ["registry.version=staging", "env=prod"],
            {"env": "prod", "registry": {"version": "staging", "pvcSize": "20Gi"}},
        ),
    ],
)
def test_set_command_line_values(
    input_dict: Dict[str, Any], values: List[str], result_dict: Dict[str, Any]
):
    set_command_line_values(input_dict, values)
    assert input_dict == result_dict


@pytest.mark.parametrize(
    "options, result_dict",
    [
        (
            ["environment=prod"],
            {"environment": "prod"},
        ),
        (
            ["number=2"],
            {"number": 2},
        ),
        (
            ["boolean=true"],
            {"boolean": True},
        ),
        (
            ["boolean=yes"],
            {"boolean": True},
        ),
        (
            ["environment=prod", "clusterName=staging"],
            {"environment": "prod", "clusterName": "staging"},
        ),
        (
            ["defaults.registry=prod"],
            {"defaults": {"registry": "prod"}},
        ),
        (
            ["services.registry.version=master"],
            {"services": {"registry": {"version": "master"}}},
        ),
    ],
)
def test_dict_from_custom_options(options, result_dict):
    assert dict_from_custom_options(options) == result_dict


@pytest.mark.parametrize(
    "input_yaml_str, keep, expected_output",
    [
        (
            """\
clusterName: backend
""",
            ["clusterName"],
            """\
clusterName: backend
""",
        ),
        (
            """\
clusterName: backend
""",
            [],
            """\
{}
""",
        ),
        (
            """\
clusterName: backend
domainName: ryax.io
""",
            ["clusterName"],
            """\
clusterName: backend
""",
        ),
        (
            """\
defaults:
  tls:
    enabled: true
""",
            ["defaults.tls.enabled"],
            """\
defaults:
  tls:
    enabled: true
""",
        ),
        (
            """\
clusterName: backend
domainName: ryax.io
environment: prod
defaults:
  registry: dev
  tls:
    enabled: true
filestore:
  enabled: true
  pvcSize: 10Gi
""",
            ["clusterName", "domainName", "defaults.tls.enabled", "filestore.pvcSize"],
            """\
clusterName: backend
defaults:
  tls:
    enabled: true
domainName: ryax.io
filestore:
  pvcSize: 10Gi
""",
        ),
    ],
)
def test_delete_unwanted_values(
    input_yaml_str: str, keep: list[str], expected_output: dict
):
    values = yaml.load(input_yaml_str, Loader=yaml.FullLoader)
    minimal_values = get_minimal_values(values, keep)
    assert yaml.dump(minimal_values) == expected_output
