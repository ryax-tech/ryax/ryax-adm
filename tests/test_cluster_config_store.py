from typing import Any, Dict

import pytest

from ryax.adm.actions.cluster_config_store import (
    _get_latest_config_from_cluster,
    _get_version,
    get_values_from_cluster,
    push_values_to_cluster,
)


@pytest.fixture
def minimal_values() -> Dict[str, Any]:
    return {
        "clusterName": "test",
        "environment": "development",
        "chartRegistry": {
            "url": "https://registry.ryax.org/chartrepo/dev",
            "user": "user",
            "pass": "pass",
        },
    }


@pytest.mark.asyncio
async def test_push_values_to_cluster(minimal_values: Dict[str, Any]):
    await push_values_to_cluster(minimal_values)

    assert minimal_values == await get_values_from_cluster()


@pytest.mark.asyncio
async def test_multiple_push_and_retreive_values(minimal_values: Dict[str, Any]):
    await push_values_to_cluster(minimal_values)
    minimal_values["clusterName"] = "v2"
    await push_values_to_cluster(minimal_values)
    minimal_values["clusterName"] = "v3"
    await push_values_to_cluster(minimal_values)

    latest_config = await get_values_from_cluster()
    assert latest_config["clusterName"] == "v3"


@pytest.mark.asyncio
async def test_change_revision_only_if_necessary(minimal_values: Dict[str, Any]):
    await push_values_to_cluster(minimal_values)
    revision_v1, _ = await _get_latest_config_from_cluster()

    minimal_values["clusterName"] = "test1"
    await push_values_to_cluster(minimal_values)
    revision_v2, config_str_2 = await _get_latest_config_from_cluster()

    assert _get_version(revision_v1) + 1 == _get_version(revision_v2)

    await push_values_to_cluster(minimal_values)
    revision_v3, config_str_3 = await _get_latest_config_from_cluster()

    assert config_str_2 == config_str_3
    assert revision_v2 == revision_v3
