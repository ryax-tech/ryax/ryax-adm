from typing import List

import pytest

from ryax.adm.cli.utils import get_string_subset


@pytest.mark.parametrize(
    "input_set, include, exclude, result",
    [
        ([], [], [], []),
        (
            ["authorization", "runner", "studio"],
            [],
            [],
            ["authorization", "runner", "studio"],
        ),
        (["authorization", "runner", "studio"], ["runner"], [], ["runner"]),
        (
            ["authorization", "runner", "studio"],
            [],
            ["runner"],
            ["authorization", "studio"],
        ),
        (["authorization", "runner", "studio"], ["studio"], ["runner"], ["studio"]),
        (["authorization", "runner", "studio"], ["runner"], ["runner"], []),
    ],
)
def test_string_subset(input_set, include, exclude, result: List[str]):
    assert set(get_string_subset(input_set, include, exclude)) == set(result)


@pytest.mark.parametrize(
    "input_set, include, exclude",
    [
        ([], ["studio"], []),
        (
            ["authorization", "runner", "studio"],
            ["repository"],
            [],
        ),
    ],
)
def test_check_include_arguments(input_set, include, exclude):
    with pytest.raises(ValueError):
        get_string_subset(input_set, include, exclude)
