from pathlib import Path
from typing import Any, Dict
from unittest import mock

import pytest
import yaml

from ryax.adm.infrastructure.external_command import V1ExternalCommand
from ryax.adm.infrastructure.helmfile import (
    HELMFILE_FILE,
    get_enabled_services,
    get_internal_namespace,
    load_helmfile_file,
    run_helmfile_command,
)


def test_load_helmfile_file():
    helmfile_d = load_helmfile_file()
    assert "releases" in helmfile_d
    assert "repositories" in helmfile_d


@pytest.mark.parametrize(
    "helmfile, namespace",
    [
        (
            {
                "environments": {
                    "default": {
                        "values": [
                            "values.yaml",
                            {"internalNamespace": "ryaxns"},
                            {"brokerName": "broker"},
                            {"registry": 5000},
                        ]
                    }
                }
            },
            "ryaxns",
        ),
        (
            {
                "environments": {
                    "default": {
                        "values": [
                            "values.yaml",
                            {"internalNamespace": "ryaxns"},
                            {
                                "monitoringNamespace": "{{ .Values.internalNamespace }}-monitoring"
                            },
                        ]
                    }
                }
            },
            "ryaxns",
        ),
        (
            {
                "environments": {
                    "default": {
                        "values": [
                            "values.yaml",
                            {"internalNamespace": "cool-namespace"},
                            {
                                "monitoringNamespace": "{{ .Values.internalNamespace }}-monitoring"
                            },
                        ]
                    }
                },
                "internalNamespace": "somethingelse",
            },
            "cool-namespace",
        ),
    ],
)
@mock.patch(
    "ryax.adm.infrastructure.helmfile.HELMFILE_FILE", "/tmp/ryax_test_helmfile.yaml"
)
def test_get_internal_namespace(helmfile: Dict[str, Any], namespace: str):
    with open("/tmp/ryax_test_helmfile.yaml", "w") as tmp_f:
        yaml.dump(helmfile, tmp_f)
        helmfile_namespace = get_internal_namespace()
        assert helmfile_namespace == namespace


# /!\ patch helmfile symbols, not this file symbols
# source https://docs.python.org/3/library/unittest.mock.html#where-to-patch
@pytest.mark.asyncio
@mock.patch("ryax.adm.infrastructure.helmfile.run_external_command")
@mock.patch("ryax.adm.infrastructure.helmfile.NamedTemporaryFile")
async def test_run_helmfile_command_no_values_file(
    tmp_file_mock, run_external_command_mock
):
    tmp_file_mock.return_value.__enter__.return_value.name = "tmpfile"

    # Wrong path gives us an error
    with pytest.raises(ValueError):
        await run_helmfile_command("", Path("/this/place/doesnt/exists"), {})
    # No path is no problem
    await run_helmfile_command("", None, {})
    run_external_command_mock.assert_awaited_with(
        V1ExternalCommand(
            f"helmfile -f {HELMFILE_FILE} --state-values-file tmpfile ",
            {},
            ["helmfile", "helm"],
        ),
        backoff_limit=0,
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "command, enabled_services, scope, skip_deps, retry, expected_command",
    [
        (
            "",
            {"studio": {}, "runner": {}, "authorization": {}},
            "",
            False,
            0,
            f"helmfile -f {HELMFILE_FILE} --state-values-file /tmp/ryax_test_values.yaml --state-values-file tmpfile ",
        ),
        (
            "apply",
            {"studio": {}, "runner": {}, "authorization": {}},
            "instance",
            True,
            0,
            f'helmfile -f {HELMFILE_FILE} -l scope="instance" --state-values-file /tmp/ryax_test_values.yaml --state-values-file tmpfile apply --skip-deps',
        ),
        (
            "",
            {
                "studio": {"enabled": True},
                "runner": {"enabled": True},
                "authorization": {"enabled": False},
            },
            "",
            False,
            0,
            f"helmfile -f {HELMFILE_FILE} --state-values-file /tmp/ryax_test_values.yaml --state-values-file tmpfile ",
        ),
    ],
)
@mock.patch("ryax.adm.infrastructure.helmfile.run_external_command")
@mock.patch("ryax.adm.infrastructure.helmfile.NamedTemporaryFile")
async def test_run_helmfile_command(
    tmpfile_mock,
    run_external_command_mock,
    command,
    enabled_services,
    scope,
    skip_deps,
    retry,
    expected_command,
):
    file_mock = mock.MagicMock()
    tmpfile_mock.return_value.__enter__.return_value = file_mock
    file_mock.name = "tmpfile"
    file_mock.write = mock.MagicMock()

    with open("/tmp/ryax_test_values.yaml", "w") as tmp_values:
        await run_helmfile_command(
            command, Path(tmp_values.name), enabled_services, scope, skip_deps, retry
        )
        run_external_command_mock.assert_called_once_with(
            V1ExternalCommand(
                expected_command,
                {},
                ["helmfile", "helm"],
            ),
            backoff_limit=0,
        )


@pytest.mark.parametrize(
    "all_services, include, exclude, result",
    [
        ([], [], [], {}),
        (
            ["studio", "runner"],
            ["studio"],
            [],
            {"studio": {"enabled": True}, "runner": {"enabled": False}},
        ),
        (
            ["studio", "runner"],
            [],
            ["studio"],
            {"studio": {"enabled": False}, "runner": {}},
        ),
        (
            ["studio", "runner"],
            ["studio"],
            ["studio"],
            {"studio": {"enabled": False}, "runner": {"enabled": False}},
        ),
        (["studio", "runner"], [], [], {"studio": {}, "runner": {}}),
    ],
)
def test_get_enabled_services(all_services, include, exclude, result):
    assert result == get_enabled_services(all_services, include, exclude)
