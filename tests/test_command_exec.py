import pytest

from ryax.adm.infrastructure.external_command import (
    CommandFailed,
    V1ExternalCommand,
    run_external_command,
    run_external_commands,
)


@pytest.mark.asyncio
async def test_running_command():
    cmd = V1ExternalCommand(
        "echo toto; echo >&2 $TEST_VAR", {"TEST_VAR": "tata"}, ["echo"]
    )
    stdout, stderr = await run_external_command(cmd)
    assert stdout == "toto\n"
    assert stderr == "tata\n"


@pytest.mark.asyncio
async def test_running_command_dry_run():
    cmd = V1ExternalCommand(
        "echo toto; echo >&2 $TEST_VAR", {"TEST_VAR": "tata"}, ["echo"]
    )
    stdout, stderr = await run_external_command(cmd, dry_run=True)
    assert stdout == ""
    assert stderr == ""


@pytest.mark.asyncio
async def test_running_command_allow_failure():
    cmd = V1ExternalCommand("false", {"TEST_VAR": "tata"}, ["echo"])
    stdout, stderr = await run_external_command(cmd, allow_failure=True)
    assert stdout == ""
    assert stderr == ""


@pytest.mark.asyncio
async def test_running_command_with_backoff():
    cmd = V1ExternalCommand("false", {"TEST_VAR": "tata"}, ["echo"])
    stdout, stderr = await run_external_command(
        cmd, backoff_limit=2, backoff_delay=0.1, allow_failure=True
    )
    assert stdout == ""
    assert stderr == ""


@pytest.mark.asyncio
async def test_running_commands():
    cmd1 = V1ExternalCommand(
        "echo cmd1; echo >&2 $TEST_VAR", {"TEST_VAR": "cmd1"}, ["echo"]
    )
    cmd2 = V1ExternalCommand(
        "echo cmd2; echo >&2 $TEST_VAR", {"TEST_VAR": "cmd2"}, ["echo"]
    )
    results = await run_external_commands([cmd1, cmd2])
    assert len(results) == 2
    assert results[0][0] == results[0][1] == "cmd1\n"
    assert results[1][0] == results[1][1] == "cmd2\n"


@pytest.mark.asyncio
async def test_running_commands_return_execptions():
    cmd1 = V1ExternalCommand(
        "echo cmd1; echo >&2 $TEST_VAR", {"TEST_VAR": "cmd1"}, ["echo"]
    )
    cmd2 = V1ExternalCommand("false", {}, [])
    results = await run_external_commands([cmd1, cmd2], return_exceptions=True)
    assert results[0][0] == results[0][1] == "cmd1\n"
    assert isinstance(results[1], CommandFailed)


@pytest.mark.asyncio
async def test_running_commands_raising():
    cmd1 = V1ExternalCommand(
        "echo cmd1; echo >&2 $TEST_VAR", {"TEST_VAR": "cmd1"}, ["echo"]
    )
    cmd2 = V1ExternalCommand("false", {}, [])
    with pytest.raises(CommandFailed) as exc:
        await run_external_commands([cmd1, cmd2])
    assert "false" in str(exc)


@pytest.mark.asyncio
async def test_running_commands_parallel():
    cmd1 = V1ExternalCommand(
        "echo cmd1; echo >&2 $TEST_VAR", {"TEST_VAR": "cmd1"}, ["echo"]
    )
    cmd2 = V1ExternalCommand(
        "echo cmd2; echo >&2 $TEST_VAR", {"TEST_VAR": "cmd2"}, ["echo"]
    )
    results = await run_external_commands([cmd1, cmd2], parallelize=True)
    assert len(results) == 2


@pytest.mark.asyncio
async def test_running_commands_parallel_return_execptions():
    cmd1 = V1ExternalCommand("false", {}, [])
    cmd2 = V1ExternalCommand("false", {}, [])
    results = await run_external_commands(
        [cmd1, cmd2], parallelize=True, return_exceptions=True
    )
    assert isinstance(results[0], CommandFailed)
    assert isinstance(results[1], CommandFailed)


@pytest.mark.asyncio
async def test_running_commands_parallel_raising():
    cmd1 = V1ExternalCommand(
        "echo cmd1; echo >&2 $TEST_VAR", {"TEST_VAR": "cmd1"}, ["echo"]
    )
    cmd2 = V1ExternalCommand("false", {}, [])
    with pytest.raises(CommandFailed) as exc:
        await run_external_commands([cmd1, cmd2], parallelize=True)
    assert "false" in str(exc)
